﻿using System;
using System.Net.Sockets;
using System.Net;
using System.Text;
using System.Threading;

namespace serv
{
    class Program
    {
        static void Main(string[] args)
        {
            var socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            var endPoint = new IPEndPoint(IPAddress.Broadcast, 15000);
            socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.Broadcast, 1);

            try
            {
                while (true)
                {
                    Console.WriteLine("try to send...");
                    byte[] data = Encoding.ASCII.GetBytes(DateTime.Now.ToString("T"));
                    socket.SendTo(data, endPoint);
                    Thread.Sleep(1000); //посмотреть опцию сокета SetTimeOut

                }
            }
            finally
            {
                socket.Close();
            }
            
        }
    }
}
