﻿using System;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace cliXP
{
    class Program
    {
        static void Main(string[] args)
        {
            var listener = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            var endPoint = new IPEndPoint(IPAddress.Any, 15000);

            try
            {
                listener.Bind(endPoint);

                byte[] buffer = new byte[256];

                while (true)
                {
                    listener.Receive(buffer);
                    Console.WriteLine(Encoding.ASCII.GetString(buffer));
                }
            }
            finally
            {
                listener.Close();
            }
        }
    }
}
