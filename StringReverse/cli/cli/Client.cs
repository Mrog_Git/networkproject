﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace cli
{
    class Client
    {
        public static void Work(string servIp)
        {
            while (true)
            {
                byte[] recivedBuffer = new byte[1024];
                Socket s = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                try
                {
                    IPEndPoint ep = new IPEndPoint(IPAddress.Parse(servIp), 15000);

                    Console.WriteLine("Connecting to server...");
                    s.Connect(ep);
                    Console.WriteLine("Connected!");
                    Console.WriteLine("Write a massage: ");
                    byte[] buffer = Encoding.ASCII.GetBytes(Console.ReadLine());
                    if (buffer.Length == 0) break;
                    s.Send(buffer);
                    Console.WriteLine("Wating response...");
                    s.Receive(recivedBuffer);
                    Console.WriteLine("Sevrers' answer: {0}", Encoding.ASCII.GetString(recivedBuffer));
                    s.Shutdown(SocketShutdown.Both);
                    s.Disconnect(false);
                }
                catch (SocketException e)
                {
                    Console.WriteLine("Connecting to server...");

                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }
                finally
                {
                    s.Close();
                }
            }
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Write server IP address: ");
            string ip = Console.ReadLine();
            Work(ip);
        }
    }
}
