﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace serv
{
    public class ServiceInfoObject
    {
        public Socket socket;
        public const int bufferSize = 1024;
        public byte[] buffer = new byte[bufferSize];
    }

    public class Server
    {
        public static ManualResetEvent threadEvent = new ManualResetEvent(false);

        static string ReverseString(string s)
        {
            for (int i=0; i < s.Length-1; i++)
                if (s[i] == ' ' && s[i + 1] == ' ') s.Remove(i, 1);

            string temp ="";
            while (s.IndexOf(' ') > -1)
            {
                if (temp != "") temp = temp.Insert(0," ");
                temp = temp.Insert(0,s.Substring(0, s.IndexOf(' ')));
                s = s.Remove(0, s.IndexOf(' ') + 1);
            }
            temp = temp.Insert(0, " ");
            temp = temp.Insert(0, s.Substring(0,s.IndexOf('\0')));
            return temp;
        }

        public static void AsyncAccept(IAsyncResult ar)
        {
            threadEvent.Set();
            Console.WriteLine("New accepting thread created...");
            ServiceInfoObject info = new ServiceInfoObject();

            Socket listener = (Socket)ar.AsyncState;
            Socket handler = listener.EndAccept(ar);
           

            try
            {
                info.socket = handler;
                info.socket.BeginReceive(info.buffer, 0, ServiceInfoObject.bufferSize, 0, new AsyncCallback(AsyncReceive), info);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                handler.Close();
                Console.WriteLine("Accepting thread closed...");
            }
        }

        public static void AsyncReceive(IAsyncResult ar)
        {
            try
            {
                ServiceInfoObject info = (ServiceInfoObject)ar.AsyncState;
                info.socket.EndReceive(ar);
                string msg = Encoding.ASCII.GetString(info.buffer);
                msg = ReverseString(msg);
                byte[] buffer = Encoding.ASCII.GetBytes(msg);
                info.socket.BeginSend(buffer, 0, buffer.Length, 0, new AsyncCallback(SendBack), info);
            }
            catch(Exception e)
            {
                Console.WriteLine(e.ToString());
                ((ServiceInfoObject)ar.AsyncState).socket.Close();
                Console.WriteLine("Accepting thread closed...");
            }
        }

        public static void SendBack(IAsyncResult ar)
        {
            try
            {
                ServiceInfoObject info = (ServiceInfoObject)ar.AsyncState;
                info.socket.EndSend(ar);
                info.socket.Shutdown(SocketShutdown.Both);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString()); 
            }
            finally
            {
                ((ServiceInfoObject)ar.AsyncState).socket.Close();
                Console.WriteLine("Accepting thread closed...");
            }
        }

        static void Main(string[] args)
        {
            Socket listner = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            try
            {
                IPEndPoint ep = new IPEndPoint(IPAddress.Any, 15000);
                listner.Bind(ep);
                listner.Listen(5);
                

                while (true)
                {
                    threadEvent.Reset();
                    listner.BeginAccept(new AsyncCallback(AsyncAccept), listner);
                    threadEvent.WaitOne();
                }
            }
            catch(Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                listner.Close();
            }
        }
    }
}
