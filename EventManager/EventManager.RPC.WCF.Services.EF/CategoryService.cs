﻿using System.Collections.Generic;
using EventManager.Model.DTO;
using EventManager.Model.EF.Entities;
using EventManager.Model.EF.Repository;
using EventManager.RPC.WCF.Interfaces;

namespace EventManager.RPC.WCF.Services.EF
{
    public class CategoryService : ICategoryService
    {
        private readonly DynamicCrudRepository _repository;

        public CategoryService()
        {
            _repository = new DynamicCrudRepository();
        }

        public CategoryDTO GetCategory(int id)
        {
            return (CategoryDTO)DTOConverter.Convert(_repository.Get(typeof(Category), id));
        }

        public List<CategoryDTO> GetAllCategorys()
        {
            return DTOConverter.Convert(_repository.GetAll(typeof(Category)));
        }

        public void AddCategory(CategoryDTO e)
        {
            _repository.Add(DTOConverter.Convert(e));
        }

        public void UpdateCategory(CategoryDTO e)
        {
            _repository.Update(DTOConverter.Convert(e));
        }

        public void DeleteCategory(CategoryDTO e)
        {
            _repository.Delete(DTOConverter.Convert(e));
        }
    }
}
