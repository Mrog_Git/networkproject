﻿using System.Collections.Generic;
using EventManager.Model.DTO;
using EventManager.Model.EF.Entities;
using EventManager.Model.EF.Repository;
using EventManager.RPC.WCF.Interfaces;

namespace EventManager.RPC.WCF.Services.EF
{
    public class PersonService : IPersonService
    {
        private readonly DynamicCrudRepository _repository;

        public PersonService()
        {
            _repository = new DynamicCrudRepository();
        }
        
        public PersonDTO GetPerson(int id)
        {
            return (PersonDTO)DTOConverter.Convert(_repository.Get(typeof(Person), id));
        }

        public List<PersonDTO> GetAllPersons()
        {
            return DTOConverter.Convert(_repository.GetAll(typeof(Person)));
        }

        public void AddPerson(PersonDTO e)
        {
            _repository.Add(DTOConverter.Convert(e));
        }

        public void UpdatePerson(PersonDTO e)
        {
            _repository.Update(DTOConverter.Convert(e));
        }

        public void DeletePerson(PersonDTO e)
        {
            _repository.Delete(DTOConverter.Convert(e));
        }
    }
}
