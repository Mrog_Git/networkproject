﻿using System.Collections.Generic;
using EventManager.Model.DTO;
using EventManager.Model.EF.Entities;
using EventManager.Model.EF.Repository;
using EventManager.RPC.WCF.Interfaces;

namespace EventManager.RPC.WCF.Services.EF
{
    public class EventService : IEventService
    {
        private readonly DynamicCrudRepository _repository;

        public EventService()
        {
            _repository = new DynamicCrudRepository();
        }

        public EventDTO GetEvent(int id)
        {
            return (EventDTO)DTOConverter.Convert(_repository.Get(typeof(Event), id));
        }

        public List<EventDTO> GetAllEvents()
        {
            return DTOConverter.Convert(_repository.GetAll(typeof(Event)));
        }

        public void AddEvent(EventDTO e)
        {
            _repository.Add(DTOConverter.Convert(e));
        }

        public void UpdateEvent(EventDTO e)
        {
            _repository.Update(DTOConverter.Convert(e));
        }

        public void DeleteEvent(EventDTO e)
        {
            _repository.Delete(DTOConverter.Convert(e));
        }
    }
}
