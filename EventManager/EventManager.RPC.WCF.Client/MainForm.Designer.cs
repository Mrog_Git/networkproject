﻿namespace EventManager.RPC.WCF.Client
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCreateEvent = new System.Windows.Forms.Button();
            this.btnCreatePerson = new System.Windows.Forms.Button();
            this.btnCreateCategory = new System.Windows.Forms.Button();
            this.btnViewEvents = new System.Windows.Forms.Button();
            this.btnViewPersons = new System.Windows.Forms.Button();
            this.btnViewCategories = new System.Windows.Forms.Button();
            this.btnUpdateEvent = new System.Windows.Forms.Button();
            this.btnUpdateCategory = new System.Windows.Forms.Button();
            this.btnUpdatePerson = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnCreateEvent
            // 
            this.btnCreateEvent.Location = new System.Drawing.Point(25, 23);
            this.btnCreateEvent.Name = "btnCreateEvent";
            this.btnCreateEvent.Size = new System.Drawing.Size(101, 23);
            this.btnCreateEvent.TabIndex = 0;
            this.btnCreateEvent.Text = "Create Event";
            this.btnCreateEvent.UseVisualStyleBackColor = true;
            this.btnCreateEvent.Click += new System.EventHandler(this.btnCreateEvent_Click);
            // 
            // btnCreatePerson
            // 
            this.btnCreatePerson.Location = new System.Drawing.Point(25, 52);
            this.btnCreatePerson.Name = "btnCreatePerson";
            this.btnCreatePerson.Size = new System.Drawing.Size(101, 23);
            this.btnCreatePerson.TabIndex = 1;
            this.btnCreatePerson.Text = "Create Person";
            this.btnCreatePerson.UseVisualStyleBackColor = true;
            this.btnCreatePerson.Click += new System.EventHandler(this.btnCreatePerson_Click);
            // 
            // btnCreateCategory
            // 
            this.btnCreateCategory.Location = new System.Drawing.Point(25, 81);
            this.btnCreateCategory.Name = "btnCreateCategory";
            this.btnCreateCategory.Size = new System.Drawing.Size(101, 23);
            this.btnCreateCategory.TabIndex = 2;
            this.btnCreateCategory.Text = "Create Category";
            this.btnCreateCategory.UseVisualStyleBackColor = true;
            this.btnCreateCategory.Click += new System.EventHandler(this.btnCreateCategory_Click);
            // 
            // btnViewEvents
            // 
            this.btnViewEvents.Location = new System.Drawing.Point(132, 23);
            this.btnViewEvents.Name = "btnViewEvents";
            this.btnViewEvents.Size = new System.Drawing.Size(105, 23);
            this.btnViewEvents.TabIndex = 3;
            this.btnViewEvents.Text = "View Events";
            this.btnViewEvents.UseVisualStyleBackColor = true;
            this.btnViewEvents.Click += new System.EventHandler(this.btnViewEvents_Click);
            // 
            // btnViewPersons
            // 
            this.btnViewPersons.Location = new System.Drawing.Point(132, 52);
            this.btnViewPersons.Name = "btnViewPersons";
            this.btnViewPersons.Size = new System.Drawing.Size(105, 23);
            this.btnViewPersons.TabIndex = 4;
            this.btnViewPersons.Text = "View Persons";
            this.btnViewPersons.UseVisualStyleBackColor = true;
            this.btnViewPersons.Click += new System.EventHandler(this.btnViewPerson_Click);
            // 
            // btnViewCategories
            // 
            this.btnViewCategories.Location = new System.Drawing.Point(132, 81);
            this.btnViewCategories.Name = "btnViewCategories";
            this.btnViewCategories.Size = new System.Drawing.Size(105, 23);
            this.btnViewCategories.TabIndex = 5;
            this.btnViewCategories.Text = "View Categories";
            this.btnViewCategories.UseVisualStyleBackColor = true;
            this.btnViewCategories.Click += new System.EventHandler(this.btnViewCategories_Click);
            // 
            // btnUpdateEvent
            // 
            this.btnUpdateEvent.Location = new System.Drawing.Point(243, 23);
            this.btnUpdateEvent.Name = "btnUpdateEvent";
            this.btnUpdateEvent.Size = new System.Drawing.Size(105, 23);
            this.btnUpdateEvent.TabIndex = 6;
            this.btnUpdateEvent.Text = "Update Event";
            this.btnUpdateEvent.UseVisualStyleBackColor = true;
            this.btnUpdateEvent.Click += new System.EventHandler(this.btnUpdateEvent_Click);
            // 
            // btnUpdateCategory
            // 
            this.btnUpdateCategory.Location = new System.Drawing.Point(243, 81);
            this.btnUpdateCategory.Name = "btnUpdateCategory";
            this.btnUpdateCategory.Size = new System.Drawing.Size(105, 23);
            this.btnUpdateCategory.TabIndex = 7;
            this.btnUpdateCategory.Text = "Update Category";
            this.btnUpdateCategory.UseVisualStyleBackColor = true;
            this.btnUpdateCategory.Click += new System.EventHandler(this.btnUpdateCategory_Click);
            // 
            // btnUpdatePerson
            // 
            this.btnUpdatePerson.Location = new System.Drawing.Point(243, 52);
            this.btnUpdatePerson.Name = "btnUpdatePerson";
            this.btnUpdatePerson.Size = new System.Drawing.Size(105, 23);
            this.btnUpdatePerson.TabIndex = 8;
            this.btnUpdatePerson.Text = "Update Person";
            this.btnUpdatePerson.UseVisualStyleBackColor = true;
            this.btnUpdatePerson.Click += new System.EventHandler(this.btnUpdatePerson_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(376, 122);
            this.Controls.Add(this.btnUpdatePerson);
            this.Controls.Add(this.btnUpdateCategory);
            this.Controls.Add(this.btnUpdateEvent);
            this.Controls.Add(this.btnViewCategories);
            this.Controls.Add(this.btnViewPersons);
            this.Controls.Add(this.btnViewEvents);
            this.Controls.Add(this.btnCreateCategory);
            this.Controls.Add(this.btnCreatePerson);
            this.Controls.Add(this.btnCreateEvent);
            this.Name = "MainForm";
            this.Text = "Event Manager";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnCreateEvent;
        private System.Windows.Forms.Button btnCreatePerson;
        private System.Windows.Forms.Button btnCreateCategory;
        private System.Windows.Forms.Button btnViewEvents;
        private System.Windows.Forms.Button btnViewPersons;
        private System.Windows.Forms.Button btnViewCategories;
        private System.Windows.Forms.Button btnUpdateEvent;
        private System.Windows.Forms.Button btnUpdateCategory;
        private System.Windows.Forms.Button btnUpdatePerson;
    }
}

