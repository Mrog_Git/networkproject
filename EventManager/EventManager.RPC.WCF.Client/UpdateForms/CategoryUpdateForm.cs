﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using EventManager.Model.DTO;
using EventManager.RPC.WCF.Client.Connected_Services.CategoryServiceReference;
using EventManager.RPC.WCF.Client.Connected_Services.EventServiceReference;

namespace EventManager.RPC.WCF.Client.UpdateForms
{
    public partial class CategoryUpdateForm : Form
    {
        private CategoryDTO _activeCategory;
        private readonly CategoryServiceClient _categoryClient;

        public CategoryUpdateForm(EventServiceClient eventClient,
            CategoryServiceClient categoryClient)
        {
            _categoryClient = categoryClient;

            InitializeComponent();

            clbEvents.DataSource = eventClient.GetAllEvents().ToList();

            Reset();
        }

        private void Reset()
        {
            tbName.Text = "";
            tbName.Enabled = false;

            clbEvents.ClearSelected();

            clbEvents.Enabled = false;
            btnDelete.Enabled = false;
            btnUpdate.Enabled = false;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(tbName.Text))
                MessageBox.Show(@"Entered data is not full", @"Enter data!",
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            else
            {
                _activeCategory.Name = tbName.Text;

                _activeCategory.Events.Clear();
                foreach (var i in clbEvents.CheckedIndices)
                {
                    _activeCategory.Events.Add((clbEvents.DataSource as List<EventDTO>)?[(int)i]);
                }

                _categoryClient.UpdateCategory(_activeCategory);

                Close();
            }
        }

        private void btnChoose_Click(object sender, EventArgs e)
        {
            try
            {
                _activeCategory = _categoryClient.GetCategory(Convert.ToInt32(tbId.Text));

                if (_activeCategory != null)
                {
                    clbEvents.Enabled = true;
                    tbName.Enabled = true;

                    tbName.Text = _activeCategory.Name;

                    for (var i = 0; i < clbEvents.Items.Count; i++)
                    {
                        clbEvents.SetItemChecked(i,
                            _activeCategory.Events.Any(m => m.ID == ((EventDTO) clbEvents.Items[i]).ID));
                    }

                    btnUpdate.Enabled = true;
                    btnDelete.Enabled = true;
                }
                else MessageBox.Show($@"There is no category with ID = {Convert.ToInt32(tbId.Text)}");

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void tbId_TextChanged(object sender, EventArgs e)
        {
            Reset();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            _categoryClient.DeleteCategory(_activeCategory);

            Close();
        }
    }
}
