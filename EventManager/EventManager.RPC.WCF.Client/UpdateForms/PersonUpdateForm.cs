﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using EventManager.Model.DTO;
using EventManager.RPC.WCF.Client.Connected_Services.EventServiceReference;
using EventManager.RPC.WCF.Client.Connected_Services.PersonServiceReference;

namespace EventManager.RPC.WCF.Client.UpdateForms
{
    public partial class PersonUpdateForm : Form
    {
        private PersonDTO _activePerson;
        private readonly PersonServiceClient _personClient;


        public PersonUpdateForm(EventServiceClient eventClient,
            PersonServiceClient personClient)
        {
            _personClient = personClient;

            InitializeComponent();

            clbCreatedEvents.DataSource = eventClient.GetAllEvents().ToList();
            clbVisitedEvents.DataSource = eventClient.GetAllEvents().ToList();

            Reset();
        }

        private void Reset()
        {
            tbName.Text = "";
            tbName.Enabled = false;

            clbCreatedEvents.ClearSelected();
            clbVisitedEvents.ClearSelected();

            clbCreatedEvents.Enabled = false;
            clbVisitedEvents.Enabled = false;
            btnDelete.Enabled = false;
            btnUpdate.Enabled = false;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(tbName.Text))
                MessageBox.Show(@"Entered data is not full", @"Enter data!",
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            else
            {
                _activePerson.Name = tbName.Text;

                _activePerson.CreatedEvents.Clear();
                foreach (var i in clbCreatedEvents.CheckedIndices)
                {
                    _activePerson.CreatedEvents.Add((clbCreatedEvents.DataSource as List<EventDTO>)?[(int)i]);
                }

                _activePerson.VisitedEvents.Clear();
                foreach (var i in clbVisitedEvents.CheckedIndices)
                {
                    _activePerson.VisitedEvents.Add((clbVisitedEvents.DataSource as List<EventDTO>)?[(int)i]);
                }

                _personClient.UpdatePerson(_activePerson);

                Close();
            }
        }

        private void btnChoose_Click(object sender, EventArgs e)
        {
            try
            {
                _activePerson = _personClient.GetPerson(Convert.ToInt32(tbId.Text));

                if (_activePerson != null)
                {
                    clbCreatedEvents.Enabled = true;
                    clbVisitedEvents.Enabled = true;
                    tbName.Enabled = true;

                    tbName.Text = _activePerson.Name;

                    for (var i = 0; i < clbCreatedEvents.Items.Count; i++)
                    {
                        clbCreatedEvents.SetItemChecked(i,
                            _activePerson.CreatedEvents.Any(m => m.ID == ((EventDTO)clbCreatedEvents.Items[i]).ID));
                    }
                    for (var i = 0; i < clbVisitedEvents.Items.Count; i++)
                    {
                        clbVisitedEvents.SetItemChecked(i,
                            _activePerson.VisitedEvents.Any(m => m.ID == ((EventDTO)clbVisitedEvents.Items[i]).ID));
                    }

                    btnUpdate.Enabled = true;
                    btnDelete.Enabled = true;
                }
                else MessageBox.Show($@"There is no category with ID = {Convert.ToInt32(tbId.Text)}");

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void tbId_TextChanged(object sender, EventArgs e)
        {
            Reset();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            _personClient.DeletePerson(_activePerson);

            Close();
        }
    }
}
