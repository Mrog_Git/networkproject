﻿using System;
using System.Windows.Forms;
using EventManager.RPC.WCF.Client.Connected_Services.CategoryServiceReference;
using EventManager.RPC.WCF.Client.Connected_Services.EventServiceReference;
using EventManager.RPC.WCF.Client.Connected_Services.PersonServiceReference;
using EventManager.RPC.WCF.Client.CreateForms;
using EventManager.RPC.WCF.Client.UpdateForms;
using EventManager.RPC.WCF.Client.ViewForms;

namespace EventManager.RPC.WCF.Client
{
    public partial class MainForm : Form
    {
        private readonly EventServiceClient _eventClient;
        private readonly PersonServiceClient _personClient;
        private readonly CategoryServiceClient _categoryClient;

        public MainForm()
        {
            InitializeComponent();
            try
            {
                _eventClient = new EventServiceClient();
                _personClient = new PersonServiceClient();
                _categoryClient = new CategoryServiceClient();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                throw;
            }
        }

        private void btnCreateEvent_Click(object sender, EventArgs e)
        {
            CreateEventFrom form = new CreateEventFrom(_eventClient,_personClient,_categoryClient);
            form.ShowDialog();
        }

        private void btnCreatePerson_Click(object sender, EventArgs e)
        {
            CreatePersonForm form = new CreatePersonForm(_eventClient, _personClient);
            form.ShowDialog();
        }

        private void btnCreateCategory_Click(object sender, EventArgs e)
        {
            CreateCategoryForm form = new CreateCategoryForm(_categoryClient) {Text = @"Category"};
            form.ShowDialog();
        }

        private void btnViewEvents_Click(object sender, EventArgs e)
        {
            ViewForm form = new ViewForm(_eventClient) {Text = @"Events"};
            form.ShowDialog();
        }

        private void btnViewPerson_Click(object sender, EventArgs e)
        {
            ViewForm form = new ViewForm(_personClient) {Text = @"Person"};
            form.ShowDialog();
        }

        private void btnUpdateEvent_Click(object sender, EventArgs e)
        {
            EventUpdateForm form = new EventUpdateForm(_eventClient,_personClient,_categoryClient) { Text = @"Person" };
            form.ShowDialog();
        }

        private void btnUpdateCategory_Click(object sender, EventArgs e)
        {
            CategoryUpdateForm form = new CategoryUpdateForm(_eventClient, _categoryClient);
            form.ShowDialog();
        }

        private void btnViewCategories_Click(object sender, EventArgs e)
        {
            ViewForm form = new ViewForm(_categoryClient) { Text = @"Category" };
            form.ShowDialog();
        }

        private void btnUpdatePerson_Click(object sender, EventArgs e)
        {
            PersonUpdateForm form = new PersonUpdateForm(_eventClient, _personClient);
            form.ShowDialog();
        }
    }
}
