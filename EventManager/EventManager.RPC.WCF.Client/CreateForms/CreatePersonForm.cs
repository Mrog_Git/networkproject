﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using EventManager.Model.DTO;
using EventManager.RPC.WCF.Client.Connected_Services.EventServiceReference;
using EventManager.RPC.WCF.Client.Connected_Services.PersonServiceReference;

namespace EventManager.RPC.WCF.Client.CreateForms
{
    public partial class CreatePersonForm : Form
    {

        private readonly PersonServiceClient _personClient; 

        public CreatePersonForm(EventServiceClient eventClient,
            PersonServiceClient personClient)
        {
            InitializeComponent();

            _personClient = personClient;

            clbEvents.DataSource = eventClient.GetAllEvents().ToList();
        }

        private void btnCancel_Click(object sender, System.EventArgs e)
        {
            Close();
        }

        private void btnCreate_Click(object sender, System.EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(tbName.Text))
                MessageBox.Show(@"Entered data is not full", @"Enter data!",
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            else
            {
                var newPerson = new PersonDTO() {Name = tbName.Text};
                foreach (var i in clbEvents.CheckedIndices)
                {
                    newPerson.VisitedEvents.Add((clbEvents.DataSource as List<EventDTO>)?[(int)i]);
                }

                _personClient.AddPerson(newPerson);

                Close();
            }
        }
    }
}
