﻿using System;
using System.Windows.Forms;
using EventManager.Model.DTO;
using EventManager.RPC.WCF.Client.Connected_Services.CategoryServiceReference;

namespace EventManager.RPC.WCF.Client.CreateForms
{
    public partial class CreateCategoryForm : Form
    {
        private readonly CategoryServiceClient _categoryClient;

        public CreateCategoryForm(CategoryServiceClient serviceClient)
        {
            _categoryClient = serviceClient;
            InitializeComponent();
        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(tbName.Text))
                MessageBox.Show(@"Entered data is not full", @"Enter data!",
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            else
            {
                var newCategory = new CategoryDTO() { Name = tbName.Text };

                _categoryClient.AddCategory(newCategory);

                Close();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
