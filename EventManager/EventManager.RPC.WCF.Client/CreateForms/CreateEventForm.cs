﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using EventManager.Model.DTO;
using EventManager.RPC.WCF.Client.Connected_Services.CategoryServiceReference;
using EventManager.RPC.WCF.Client.Connected_Services.EventServiceReference;
using EventManager.RPC.WCF.Client.Connected_Services.PersonServiceReference;

namespace EventManager.RPC.WCF.Client.CreateForms
{
    public partial class CreateEventFrom : Form
    {
        private readonly EventServiceClient _eventClient;

        public CreateEventFrom(EventServiceClient eventClient, 
            PersonServiceClient personClient, 
            CategoryServiceClient categoryClient)
        {
            _eventClient = eventClient;

            InitializeComponent();

            cbCategory.DataSource = categoryClient.GetAllCategorys().ToList();
            cbCreator.DataSource = personClient.GetAllPersons().ToList();
            clbMembers.DataSource = personClient.GetAllPersons().ToList();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(tbTitle.Text) ||
                cbCategory.SelectedIndex == -1 ||
                cbCreator.SelectedIndex == -1)
                MessageBox.Show(@"Entered data is not full", @"Enter data!",
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            else
            {
                var newEvent = new EventDTO()
                {
                    Title = tbTitle.Text,
                    Category_ID = chNoCategory.Checked
                        ? null
                        : (cbCategory.DataSource as List<CategoryDTO>)?[cbCategory.SelectedIndex]?.ID,
                    Creator_ID = chNoCreator.Checked
                        ? null
                        : (cbCreator.DataSource as List<PersonDTO>)?[cbCreator.SelectedIndex]?.ID
                };
                foreach (var i in clbMembers.CheckedIndices)
                {
                    newEvent.Members.Add((clbMembers.DataSource as List<PersonDTO>)?[(int)i]);
                }

                _eventClient.AddEvent(newEvent);

                var list = _eventClient.GetAllEvents().ToList();

                Close();
            }
        }

        private void chNoCreator_CheckedChanged(object sender, EventArgs e)
        {
            cbCreator.Enabled = !((CheckBox) sender).Checked;
        }

        private void chNoCategory_CheckedChanged(object sender, EventArgs e)
        {
            cbCategory.Enabled = !((CheckBox)sender).Checked;
        }
    }
}
