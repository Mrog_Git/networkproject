﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using EventManager.Model.DTO;
using EventManager.RPC.WCF.Client.Connected_Services.CategoryServiceReference;
using EventManager.RPC.WCF.Client.Connected_Services.EventServiceReference;
using EventManager.RPC.WCF.Client.Connected_Services.PersonServiceReference;

namespace EventManager.RPC.WCF.Client.ViewForms
{
    public partial class ViewForm : Form
    {
        private enum Entity
        {
            Event,
            Person,
            Category
        }

        private readonly Entity _activeEntity;
        private readonly EventServiceClient _eventClient;
        private readonly CategoryServiceClient _categoryClient;
        private readonly PersonServiceClient _personClient;

        public ViewForm(EventServiceClient client)
        {
            _eventClient = client;
            InitializeComponent();
            _activeEntity = Entity.Event;
        }

        public ViewForm(CategoryServiceClient client)
        {
            _categoryClient = client;
            InitializeComponent();
            _activeEntity = Entity.Category;
        }

        public ViewForm(PersonServiceClient client)
        {
            _personClient = client;
            InitializeComponent();
            _activeEntity = Entity.Person;
        }

        private void btnAll_Click(object sender, EventArgs e)
        {
            try
            {
                int depth = Convert.ToInt32(tbDepth.Text);

                tvEvents.Nodes.Clear();
                if (_activeEntity == Entity.Event) AddEventsToTree(depth);
                else if (_activeEntity == Entity.Category) AddCategoriesToTree(depth);
                else AddPersonsToTree(depth);
                tvEvents.Invalidate();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
            
        }

        private void AddEventsToTree(int depth)
        {
            List<EventDTO> source = _eventClient.GetAllEvents().ToList();
            tvEvents.Nodes.AddRange(source.Visualize(depth));
        }

        private void AddPersonsToTree(int depth)
        {
            List<PersonDTO> source = _personClient.GetAllPersons().ToList();
            tvEvents.Nodes.AddRange(source.Visualize(depth));
        }

        private void AddCategoriesToTree(int depth)
        {
            List<CategoryDTO> source = _categoryClient.GetAllCategorys().ToList();
            tvEvents.Nodes.AddRange(source.Visualize(depth));
        }

        private void btnId_Click(object sender, EventArgs e)
        {
            try
            {
                int depth = Convert.ToInt32(tbDepth.Text);
                int id = Convert.ToInt32(tbId.Text);

                tvEvents.Nodes.Clear();
                if (_activeEntity == Entity.Event) AddEventToTree(depth,id);
                else if (_activeEntity == Entity.Category) AddCategoryToTree(depth,id);
                else AddPersonToTree(depth,id);
                tvEvents.Invalidate();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        private void AddEventToTree(int depth, int id)
        {
            var evt = _eventClient.GetEvent(id);
            if (evt == null) MessageBox.Show($@"There is no event with ID = {id}");
            else tvEvents.Nodes.Add(evt.Visualize(depth));

        }

        private void AddPersonToTree(int depth, int id)
        {
            var evt = _personClient.GetPerson(id);
            if (evt == null) MessageBox.Show($@"There is no event with ID = {id}");
            else tvEvents.Nodes.Add(evt.Visualize(depth));

        }

        private void AddCategoryToTree(int depth, int id)
        {
            var evt = _categoryClient.GetCategory(id);
            if (evt == null) MessageBox.Show($@"There is no event with ID = {id}");
            else tvEvents.Nodes.Add(evt.Visualize(depth));

        }
    }
}
