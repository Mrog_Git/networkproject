﻿using System.Collections.Generic;
using System.ServiceModel;
using EventManager.Model.DTO;

namespace EventManager.RPC.WCF.Interfaces
{
    [ServiceContract]
    public interface IPersonService
    {
        [OperationContract]
        PersonDTO GetPerson(int id);

        [OperationContract]
        List<PersonDTO> GetAllPersons();

        [OperationContract]
        void AddPerson(PersonDTO e);

        [OperationContract]
        void UpdatePerson(PersonDTO e);

        [OperationContract]
        void DeletePerson(PersonDTO e);
    }
}
