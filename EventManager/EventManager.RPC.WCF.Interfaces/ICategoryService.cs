﻿using System.Collections.Generic;
using System.ServiceModel;
using EventManager.Model.DTO;

namespace EventManager.RPC.WCF.Interfaces
{
    [ServiceContract]
    public interface ICategoryService
    {
        [OperationContract]
        CategoryDTO GetCategory(int id);

        [OperationContract]
        List<CategoryDTO> GetAllCategorys();

        [OperationContract]
        void AddCategory(CategoryDTO e);

        [OperationContract]
        void UpdateCategory(CategoryDTO e);

        [OperationContract]
        void DeleteCategory(CategoryDTO e);
    }
}
