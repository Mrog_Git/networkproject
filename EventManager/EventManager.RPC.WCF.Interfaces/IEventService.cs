﻿using System.Collections.Generic;
using System.ServiceModel;
using EventManager.Model.DTO;

namespace EventManager.RPC.WCF.Interfaces
{
    [ServiceContract]
    public interface IEventService
    {
        [OperationContract]
        EventDTO GetEvent(int id);

        [OperationContract]
        List<EventDTO> GetAllEvents();

        [OperationContract]
        void AddEvent(EventDTO e);

        [OperationContract]
        void UpdateEvent(EventDTO e);

        [OperationContract]
        void DeleteEvent(EventDTO e);

    }
}
