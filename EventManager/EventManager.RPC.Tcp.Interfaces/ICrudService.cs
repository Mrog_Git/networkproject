﻿namespace EventManager.RPC.Tcp.Interfaces
{
    public interface ICrudService
    {
        object Get(GetEntityInfo info);

        dynamic GetAll(GetEntityInfo info);

        void Add(object entity);

        void Update(object entity);

        void Delete(object entity);
    }
}
