﻿using System;

namespace EventManager.RPC.Tcp.Interfaces
{
    public class GetEntityInfo
    {
        public Type EntityType;
        public object[] KeyParams;

        public GetEntityInfo()
        {
                
        }

        public GetEntityInfo(Type type, object[] keyParams)
        {
            EntityType = type;
            KeyParams = keyParams;
        }

        public GetEntityInfo(Type type)
        {
            EntityType = type;
            KeyParams = null;
        }
    }

    public class Command
    {
        public Command(string mathodName, object param)
        {
            MethodName = mathodName;
            Param = param;
        }

        public string MethodName { get; set; }

        public object Param { get; set; }
    }
}
