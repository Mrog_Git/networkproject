using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace EventManager.Model.DTO
{
    [DataContract(IsReference = true)]
    [KnownType(typeof(EventDTO))]
    [Serializable]
    public class CategoryDTO
    {
        public CategoryDTO()
        {
            Events = new List<EventDTO>();
        }

        [DataMember]
        public int ID { get; set; }

        [Required]
        [StringLength(50)]
        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public List<EventDTO> Events { get; set; }

        public override string ToString()
        {
            return Name + $" (ID = {ID})";
        }
    }
}
