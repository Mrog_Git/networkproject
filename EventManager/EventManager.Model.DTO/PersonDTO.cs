using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace EventManager.Model.DTO
{
    [DataContract(IsReference = true)]
    [KnownType(typeof(EventDTO))]
    [Serializable]
    [Table("Persons")]
    public class PersonDTO
    {
        public PersonDTO()
        {
            CreatedEvents = new List<EventDTO>();
            VisitedEvents = new List<EventDTO>();
        }

        [DataMember]
        public int ID { get; set; }

        [Required]
        [StringLength(50)]
        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public List<EventDTO> CreatedEvents { get; set; }

        [DataMember]
        public List<EventDTO> VisitedEvents { get; set; }

        public override string ToString()
        {
            return Name + $" (ID = {ID})";
        }
    }
}
