using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace EventManager.Model.DTO
{
    [DataContract(IsReference = true)]
    [KnownType(typeof(CategoryDTO))]
    [KnownType(typeof(PersonDTO))]
    [Serializable]
    public class EventDTO
    {
        public EventDTO()
        {
            Members = new List<PersonDTO>();
        }

        [DataMember]
        public int ID { get; set; }

        [Required]
        [StringLength(50)]
        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public int? Creator_ID { get; set; }

        [DataMember]
        public int? Category_ID { get; set; }

        [DataMember]
        public CategoryDTO Category { get; set; }

        [DataMember]
        public PersonDTO Creator { get; set; }

        [DataMember]
        public List<PersonDTO> Members { get; set; }

        public override string ToString()
        {
            return Title + $" (ID = {ID})";
        }
    }
}
