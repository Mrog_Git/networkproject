﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using EventManager.Model.EF.Entities;

namespace EventManager.Model.DTO
{
    public static class DTOConverter
    {
        private static Dictionary<Type, Type> types;

        public static void Initialize()
        {
            types = new Dictionary<Type, Type>()
            {
                {typeof(EventDTO), typeof(Event)},
                {typeof(Event), typeof(EventDTO)},
                {typeof(CategoryDTO), typeof(Category)},
                {typeof(Category), typeof(CategoryDTO)},
                {typeof(PersonDTO), typeof(Person)},
                {typeof(Person), typeof(PersonDTO)},
                {typeof(List<Event>), typeof(List<EventDTO>)},
                {typeof(List<Category>), typeof(List<CategoryDTO>)},
                {typeof(List<Person>), typeof(List<PersonDTO>)},
            };

            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<EventDTO, Event>().PreserveReferences();
                cfg.CreateMap<Event, EventDTO>().PreserveReferences();
                cfg.CreateMap<Person, PersonDTO>().PreserveReferences();
                cfg.CreateMap<PersonDTO, Person>().PreserveReferences();
                cfg.CreateMap<Category, CategoryDTO>().PreserveReferences();
                cfg.CreateMap<CategoryDTO, Category>().PreserveReferences();
            });

        }

        public static Type GetRelatedType(Type type)
        {
            Type src = types.ContainsKey(type)
                ? type
                : types.Keys.First(type.IsSubclassOf);
            return src == null ? null : types[src];
        }

        public static object Convert(object entity)
        {
            if (entity == null) return null;
            Type src = types.ContainsKey(entity.GetType())
                ? entity.GetType()
                : types.Keys.First(k => (entity.GetType()).IsSubclassOf(k));
            if (src == null) return null;
            return Mapper.Map(entity, src, types[src]);
        }
    }
}
