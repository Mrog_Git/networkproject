﻿using System;
using System.Collections.Generic;
using EventManager.Model.DTO;
using EventManager.Model.EF.Entities;
using EventManager.Model.EF.Repository;
using EventManager.WebApi.Interfaces;

namespace EventManager.WebApi.Tcp.Controllers
{
    public class PeopleController
    {
        private readonly CrudRepository<Person> _repository = new CrudRepository<Person>();

        public HttpResponse Get()
        {
            return new HttpResponse(StatusCode.Ok, (List<PersonDTO>)DTOConverter.Convert(_repository.GetAll()));
        }

        public HttpResponse Get(int id)
        {
            var e = _repository.Get(id);
            if (e != null) return new HttpResponse(StatusCode.Ok, (PersonDTO)DTOConverter.Convert(e));
            return new HttpResponse(StatusCode.NotFound);
        }

        public HttpResponse Post(PersonDTO person)
        {
            try
            {
                _repository.Add((Person)DTOConverter.Convert(person));
                return new HttpResponse(StatusCode.Ok);
            }
            catch (Exception e)
            {
                return new HttpResponse(StatusCode.BadRequest, e.Message);
            }
        }

        public HttpResponse Put(int id, PersonDTO person)
        {
            if (id != person.ID) return new HttpResponse(StatusCode.BadRequest);

            try
            {
                _repository.Update((Person)DTOConverter.Convert(person));
                var updatedPerson = _repository.Get(id);
                return new HttpResponse(StatusCode.Ok);
            }
            catch (Exception e)
            {
                if (_repository.Get(id) == null) return new HttpResponse(StatusCode.NotFound);
                return new HttpResponse(StatusCode.BadRequest, e.Message);
            }
        }

        public HttpResponse Delete(int id)
        {
            var person = _repository.Get(id);
            if (person == null) return new HttpResponse(StatusCode.NotFound);
            try
            {
                _repository.Delete(person);
                return new HttpResponse(StatusCode.Ok);
            }
            catch (Exception e)
            {
                if (_repository.Get(id) == null) return new HttpResponse(StatusCode.NotFound);
                return new HttpResponse(StatusCode.BadRequest, e.Message);
            }
        }
    }
}