﻿using System;
using System.Collections.Generic;
using EventManager.Model.DTO;
using EventManager.Model.EF.Entities;
using EventManager.Model.EF.Repository;
using EventManager.WebApi.Interfaces;

namespace EventManager.WebApi.Tcp.Controllers
{
    public class CategoriesController 
    {
        private readonly CrudRepository<Category> _repository = new CrudRepository<Category>();
        
        public HttpResponse Get()
        {
            return new HttpResponse(StatusCode.Ok, (List<CategoryDTO>)DTOConverter.Convert(_repository.GetAll()));
        }

        public HttpResponse Get(int id)
        {
            var e = _repository.Get(id);
            if (e != null) return new HttpResponse(StatusCode.Ok, (CategoryDTO)DTOConverter.Convert(e));
            return new HttpResponse(StatusCode.NotFound);
        }
        
        public HttpResponse Post(CategoryDTO category)
        {
            try
            {
                _repository.Add((Category)DTOConverter.Convert(category));
                return new HttpResponse(StatusCode.Ok);
            }
            catch(Exception e)
            {
                return new HttpResponse(StatusCode.BadRequest, e.Message);
            }
        }

        public HttpResponse Put(int id, CategoryDTO category)
        {
            if (id != category.ID) return new HttpResponse(StatusCode.BadRequest);

            try
            {
                _repository.Update((Category)DTOConverter.Convert(category));
                return new HttpResponse(StatusCode.Ok);
            }
            catch (Exception e)
            {
                if (_repository.Get(id) == null) return new HttpResponse(StatusCode.NotFound);
                return new HttpResponse(StatusCode.BadRequest, e.Message);
            }
        }

        public HttpResponse Delete(int id)
        {
            var category = _repository.Get(id);
            if (category == null) return new HttpResponse(StatusCode.NotFound);
            try
            {
                _repository.Delete(category);
                return new HttpResponse(StatusCode.Ok);
            }
            catch (Exception e)
            {
                if (_repository.Get(id) == null) return new HttpResponse(StatusCode.NotFound);
                return new HttpResponse(StatusCode.BadRequest, e.Message);
            }
        }
    }
}