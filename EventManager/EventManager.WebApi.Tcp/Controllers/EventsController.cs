﻿using System;
using System.Collections.Generic;
using EventManager.Model.DTO;
using EventManager.Model.EF.Entities;
using EventManager.Model.EF.Repository;
using EventManager.WebApi.Interfaces;

namespace EventManager.WebApi.Tcp.Controllers
{
    public class EventsController 
    {
        private readonly CrudRepository<Event> _repository = new CrudRepository<Event>();

        public HttpResponse Get()
        {
            return new HttpResponse(StatusCode.Ok, (List<EventDTO>)DTOConverter.Convert(_repository.GetAll()));
        }

        public HttpResponse Get(int id)
        {
            var e = _repository.Get(id);
            if (e != null) return new HttpResponse(StatusCode.Ok, (EventDTO)DTOConverter.Convert(e));
            return new HttpResponse(StatusCode.NotFound);
        }

        public HttpResponse Post(EventDTO evt)
        {
            try
            {
                _repository.Add((Event)DTOConverter.Convert(evt));
                return new HttpResponse(StatusCode.Ok);
            }
            catch (Exception e)
            {
                return new HttpResponse(StatusCode.BadRequest, e.Message);
            }
        }

        public HttpResponse Put(int id, EventDTO evt)
        {
            if (id != evt.ID) return new HttpResponse(StatusCode.BadRequest);

            try
            {
                _repository.Update((Event)DTOConverter.Convert(evt));
                return new HttpResponse(StatusCode.Ok);
            }
            catch (Exception e)
            {
                if (_repository.Get(id) == null) return new HttpResponse(StatusCode.NotFound);
                return new HttpResponse(StatusCode.BadRequest, e.Message);
            }
        }

        public HttpResponse Delete(int id)
        {
            var evt = _repository.Get(id);
            if (evt == null) return new HttpResponse(StatusCode.NotFound);
            try
            {
                _repository.Delete(evt);
                return new HttpResponse(StatusCode.Ok);
            }
            catch (Exception e)
            {
                if (_repository.Get(id) == null) return new HttpResponse(StatusCode.NotFound);
                return new HttpResponse(StatusCode.BadRequest, e.Message);
            }
        }
    }
}