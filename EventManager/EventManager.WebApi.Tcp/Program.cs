﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using EventManager.Model.DTO;
using EventManager.WebApi.Core;
using EventManager.WebApi.Interfaces;

namespace EventManager.WebApi.Tcp
{
    internal class Client : IDisposable
    {
        private readonly TcpClient _client;

        public Client(TcpClient client)
        {
            _client = client;
        }

        public void Dispose()
        {
            _client.Close();
        }

        private async Task<string> ReadFromStreamAsync()
        {
            var buffer = new byte[_client.ReceiveBufferSize];
            int size = await _client.GetStream().ReadAsync(buffer, 0, buffer.Length);
            return Encoding.ASCII.GetString(buffer, 0, size);
        }

        private async Task WriteToStreamAsync(string response)
        {
            var buffer = Encoding.UTF8.GetBytes(response);
            await _client.GetStream().WriteAsync(buffer,0,buffer.Length);
        }

        public async Task ProcessAsync(IWebApiServer server)
        {
            var request = await ReadFromStreamAsync();
            string response = await server.ProcessRequest(request);
            await WriteToStreamAsync(response);
        }
    }

    internal static class Program
    {
        private static readonly HashSet<Task> ActiveClientTasks = new HashSet<Task>();
        private static readonly HashSet<Type> Controllers;

        static Program()
        {
            Controllers =
                new HashSet<Type>(
                    Assembly.GetExecutingAssembly()
                        .GetTypes()
                        .Where(t => t.IsClass && t.Namespace == "EventManager.WebApi.Tcp.Controllers"));
        }

        private static void Main(string[] args)
        {
            DTOConverter.Initialize();

            var listener = new TcpListener(IPAddress.Any, 15001);

            listener.Start();

            Console.WriteLine("Press ESC to stop.");
            do
            {
                while (!Console.KeyAvailable)
                {
                    var tcpClient = listener.AcceptTcpClient();
                    tcpClient.ReceiveBufferSize = 10485760;
                    tcpClient.SendBufferSize = 10485760;
                    ProcessClient(tcpClient);
                }
            } while (Console.ReadKey(true).Key != ConsoleKey.Escape);

            listener.Stop();
            Task.WaitAll(ActiveClientTasks.ToArray());
        }

        private static async void ProcessClient(TcpClient tcpClient)
        {
            using (var client = new Client(tcpClient))
            {
                Task task = null;
                try
                {
                    task = client.ProcessAsync(new WebApiServer(Controllers));
                    ActiveClientTasks.Add(task);
                    await task;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
                finally
                {
                    if (task != null)
                        ActiveClientTasks.Remove(task);
                }
            }
        }
    }
}
