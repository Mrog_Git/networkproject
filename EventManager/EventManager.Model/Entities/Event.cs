using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EventManager.Model.EF.Entities
{
    public partial class Event
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Event()
        {
            Members = new HashSet<Person>();
        }

        public int ID { get; set; }

        [Required]
        [StringLength(50)]
        public string Title { get; set; }

        public int? Creator_ID { get; set; }

        public int? Category_ID { get; set; }

        public virtual Category Category { get; set; }

        public virtual Person Creator { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Person> Members { get; set; }
    }
}
