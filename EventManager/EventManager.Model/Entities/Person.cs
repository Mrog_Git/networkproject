using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EventManager.Model.EF.Entities
{
    [Table("Persons")]
    public partial class Person
    {
        public Person()
        {
            CreatedEvents = new HashSet<Event>();
            VisitedEvents = new HashSet<Event>();
        }

        public int ID { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        public virtual ICollection<Event> CreatedEvents { get; set; }

        public virtual ICollection<Event> VisitedEvents { get; set; }
    }
}
