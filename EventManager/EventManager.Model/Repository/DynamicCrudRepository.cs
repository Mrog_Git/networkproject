﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Metadata.Edm;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Reflection;

namespace EventManager.Model.EF.Repository
{
    public sealed class DynamicCrudRepository
    {
        private readonly EventManagerDbContext _context = new EventManagerDbContext();

        private bool IsNavigationProperty(PropertyInfo prop, Type entityType)
        {
            return NavigationProperties(entityType).Any(n => n.Name == prop.Name);
        }

        private IEnumerable<NavigationProperty> NavigationProperties(Type entityType)
        {
            var workspace = ((IObjectContextAdapter)_context).ObjectContext.MetadataWorkspace;
            var itemCollection = (ObjectItemCollection)(workspace.GetItemCollection(DataSpace.OSpace));
            return
                itemCollection.OfType<EntityType>()
                    .Single(e => itemCollection.GetClrType(e) == entityType)
                    .DeclaredNavigationProperties;
        }

        private object[] KeyParams(EntityType type, object entity)
        {
            return entity.GetType()
                .GetProperties()
                .Where(p => type.KeyMembers.Any(k => k.Name == p.Name))
                .Select(p => p.GetValue(entity)).ToArray();
        }

        private object[] KeyParams(object entity)
        {
            var workspace = ((IObjectContextAdapter)_context).ObjectContext.MetadataWorkspace;
            var itemCollection = (ObjectItemCollection)(workspace.GetItemCollection(DataSpace.OSpace));
            var type = itemCollection.OfType<EntityType>()
                .Single(e => itemCollection.GetClrType(e) == entity.GetType());
            return entity.GetType()
                .GetProperties()
                .Where(p => type.KeyMembers.Any(k => k.Name == p.Name))
                .Select(p => p.GetValue(entity)).ToArray();
        }

        private bool SameEntities(object entity1, object entity2, EntityType entityType)
        {
            var keyParams1 = KeyParams(entityType, entity1);
            var keyParams2 = KeyParams(entityType, entity2);
            return !keyParams1.Where((t, i) => !t.Equals(keyParams2[i])).Any();
        }

        private static bool IsEntityType(Type type)
        {
            using (var ctx = new EventManagerDbContext())
            {
                var workspace = ((IObjectContextAdapter)ctx).ObjectContext.MetadataWorkspace;
                var itemCollection = (ObjectItemCollection)(workspace.GetItemCollection(DataSpace.OSpace));
                return itemCollection.OfType<EntityType>().Any(e => itemCollection.GetClrType(e) == type);
            }
        }

        public void Add(object entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }
            if (!IsEntityType(entity.GetType())) throw new ArgumentException("Type of argument is not one of entities types.");

            foreach (var prop in entity.GetType().GetProperties())
            {
                if (!IsNavigationProperty(prop,entity.GetType())) continue;

                var propValue = prop.GetValue(entity) as IEnumerable;
                if (propValue == null) continue;
                using (var findCtx = new EventManagerDbContext())
                {
                    foreach (var obj in propValue)
                    {
                        if (findCtx.Set(obj.GetType()).Find(KeyParams(obj)) != null) _context.Set(obj.GetType()).Attach(obj);
                        else throw new InvalidOperationException("One of related entities does not exist in the database.");
                    }
                }                
            }
            _context.Set(entity.GetType()).Add(entity);
            _context.SaveChanges();
        }

        public void Delete(object entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }
            if (!IsEntityType(entity.GetType())) throw new ArgumentException("Type of argument is not one of entities types.");

            _context.Entry(entity).State = EntityState.Deleted;
            _context.SaveChanges();
        }

        public object Get(Type entityType, params object[] keyValues)
        {
            if (!IsEntityType(entityType)) throw new ArgumentException("Passed type is not one of entities types.");
            return _context.Set(entityType).Find(keyValues);
        }

        public dynamic GetAll(Type entityType)
        {
            if (!IsEntityType(entityType)) throw new ArgumentException("Passed type is not one of entities types.");
            dynamic prop =
                _context.GetType()
                    .GetProperties()
                    .Single(
                        p =>
                            p.PropertyType.IsGenericType &&
                            (p.PropertyType.GetGenericTypeDefinition() == typeof(DbSet<>)) &&
                            p.PropertyType.GetGenericArguments().Any(a => a == entityType))
                    .GetValue(_context);
                
            dynamic list = Enumerable.ToList(prop);
            return list;
        }

        public void Update(object entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }
            if (!IsEntityType(entity.GetType())) throw new ArgumentException("Type of argument is not one of entities types.");

            var workspace = ((IObjectContextAdapter)_context).ObjectContext.MetadataWorkspace;
            var itemCollection = (ObjectItemCollection)(workspace.GetItemCollection(DataSpace.OSpace));
            var type = itemCollection.OfType<EntityType>()
                .Single(e => itemCollection.GetClrType(e) == entity.GetType());
            object[] keyParams = KeyParams(type, entity);

            object contextEntity = _context.Set(entity.GetType()).Find(keyParams);
            if (contextEntity == null) return;

            _context.Entry(contextEntity).CurrentValues.SetValues(entity);

            foreach (var property in type.DeclaredNavigationProperties)
            {
                dynamic contextEntityProperty =
                    ((object)contextEntity).GetType().GetProperties().Single(p => p.Name == property.Name)
                        .GetValue(contextEntity);
                if (!(contextEntityProperty is IEnumerable)) continue;
                Type childType = ((IEnumerable)contextEntityProperty).AsQueryable().ElementType;
                EntityType childEntityType = itemCollection.OfType<EntityType>()
                .Single(e => itemCollection.GetClrType(e) == childType);

                dynamic entityProperty =
                    entity.GetType().GetProperties().Single(p => p.Name == property.Name).GetValue(entity);

                foreach (dynamic childEntity in Enumerable.ToList(contextEntityProperty))
                {
                    if (((IEnumerable<object>)entityProperty).All(
                            e => !SameEntities((object)childEntity, e, childEntityType)))
                        contextEntityProperty.Remove(childEntity);
                }

                foreach (var childEntity in Enumerable.ToList(entityProperty))
                {
                    if (((IEnumerable<object>)contextEntityProperty).Any(
                            e => SameEntities((object)childEntity, e, childEntityType)))
                        continue;

                    var newChild = _context.Set(childType).Find(KeyParams(childEntityType, childEntity));
                    if (newChild != null) contextEntityProperty.Add(newChild);
                    else throw new InvalidOperationException("One of related entities does not exist in the database.");
                }
            }
            _context.Entry(contextEntity).State = EntityState.Modified;
            _context.SaveChanges();
        }
    }
}
