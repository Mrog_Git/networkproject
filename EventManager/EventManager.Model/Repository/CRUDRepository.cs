﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Metadata.Edm;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Reflection;

namespace EventManager.Model.EF.Repository
{
    public class CrudRepository<T> where T: class, new() 
    {
        protected readonly EventManagerDbContext Context = new EventManagerDbContext();

        private bool IsNavigationProperty(PropertyInfo prop)
        {
            return NavigationProperties().Any(n => n.Name == prop.Name);
        }

        private IEnumerable<NavigationProperty> NavigationProperties()
        {
            var workspace = ((IObjectContextAdapter)Context).ObjectContext.MetadataWorkspace;
            var itemCollection = (ObjectItemCollection)(workspace.GetItemCollection(DataSpace.OSpace));
            return
                itemCollection.OfType<EntityType>()
                    .Single(e => itemCollection.GetClrType(e) == typeof(T))
                    .DeclaredNavigationProperties;
        }

        private object[] KeyParams(EntityType type, object entity)
        {
            return entity.GetType()
                .GetProperties()
                .Where(p => type.KeyMembers.Any(k => k.Name == p.Name))
                .Select(p => p.GetValue(entity)).ToArray();
        }

        private object[] KeyParams(object entity)
        {
            var workspace = ((IObjectContextAdapter)Context).ObjectContext.MetadataWorkspace;
            var itemCollection = (ObjectItemCollection)(workspace.GetItemCollection(DataSpace.OSpace));
            var type = itemCollection.OfType<EntityType>()
                .Single(e => itemCollection.GetClrType(e) == entity.GetType());
            return entity.GetType()
                .GetProperties()
                .Where(p => type.KeyMembers.Any(k => k.Name == p.Name))
                .Select(p => p.GetValue(entity)).ToArray();
        }

        private bool SameEntities(object entity1, object entity2, EntityType entityType)
        {
            var keyParams1 = KeyParams(entityType, entity1);
            var keyParams2 = KeyParams(entityType, entity2);
            return !keyParams1.Where((t, i) => !t.Equals(keyParams2[i])).Any();
        }

        public virtual void Add(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }
            foreach (var prop in entity.GetType().GetProperties())
            {
                if (!IsNavigationProperty(prop)) continue;

                dynamic propValue = prop.GetValue(entity);
                if (!(propValue is IEnumerable)) continue;
                foreach (var obj in Enumerable.ToList(propValue))
                {
                    var ctxObj = Context.Set(obj.GetType()).Find(KeyParams(obj));
                    if (ctxObj != null)
                    {
                        propValue.Remove(obj);
                        propValue.Add(ctxObj);
                    }
                    else throw new InvalidOperationException("One of related entities does not exist in the database.");
                }
            }
            Context.Set<T>().Add(entity);
            Context.SaveChanges();
        }

        public virtual void Delete(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }
            Context.Entry(entity).State = EntityState.Deleted;
            Context.SaveChanges();
        }

        public virtual T Get(params object[] keyValues)
        {
            return Context.Set<T>().Find(keyValues);
        }

        public virtual List<T> GetAll()
        {
            return Context.Set<T>().ToList();
        }

        public virtual void Update(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }

            var workspace = ((IObjectContextAdapter)Context).ObjectContext.MetadataWorkspace;
            var itemCollection = (ObjectItemCollection)(workspace.GetItemCollection(DataSpace.OSpace));
            var type = itemCollection.OfType<EntityType>()
                .Single(e => itemCollection.GetClrType(e) == typeof(T));
            object[] keyParams = KeyParams(type, entity);

            T contextEntity = Context.Set<T>().Find(keyParams);
            if (contextEntity == null) return;

            Context.Entry(contextEntity).CurrentValues.SetValues(entity);

            foreach (var property in type.DeclaredNavigationProperties)
            {
                dynamic contextEntityProperty =
                    contextEntity.GetType().GetProperties().Single(p => p.Name == property.Name)
                        .GetValue(contextEntity);
                if (!(contextEntityProperty is IEnumerable)) continue;
                Type childType = ((IEnumerable) contextEntityProperty).AsQueryable().ElementType;
                EntityType childEntityType = itemCollection.OfType<EntityType>()
                .Single(e => itemCollection.GetClrType(e) == childType);

                dynamic entityProperty =
                    entity.GetType().GetProperties().Single(p => p.Name == property.Name).GetValue(entity);

                foreach (dynamic childEntity in Enumerable.ToList(contextEntityProperty))
                {
                    if (((IEnumerable<object>) entityProperty).All(
                            e => !SameEntities((object) childEntity, e, childEntityType)))
                        contextEntityProperty.Remove(childEntity);
                }

                foreach (var childEntity in Enumerable.ToList(entityProperty))
                {
                    if (((IEnumerable<object>) contextEntityProperty).Any(
                            e => SameEntities((object) childEntity, e, childEntityType)))
                        continue;

                    var newChild = Context.Set(childType).Find(KeyParams(childEntityType, childEntity));
                    if (newChild != null) contextEntityProperty.Add(newChild);
                    else throw new InvalidOperationException("One of related entities does not exist in the database.");
                }
            }
            Context.Entry(contextEntity).State = EntityState.Modified;
            Context.SaveChanges();
        }
    }
}
