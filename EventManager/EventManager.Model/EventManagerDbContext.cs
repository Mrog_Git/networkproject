using System.Data.Entity;
using EventManager.Model.EF.Entities;

namespace EventManager.Model.EF
{
    public partial class EventManagerDbContext : DbContext
    {
        public EventManagerDbContext()
            : base(@"data source=MARTIN\SQLEXPRESS;initial catalog=SimpleEventDB;integrated security=True;MultipleActiveResultSets=True;App=EntityFramework")
        {
            
        }

        public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<Event> Events { get; set; }
        public virtual DbSet<Person> Persons { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Category>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Category>()
                .HasMany(e => e.Events)
                .WithOptional(e => e.Category)
                .HasForeignKey(e => e.Category_ID)
                .WillCascadeOnDelete();

            modelBuilder.Entity<Event>()
                .Property(e => e.Title)
                .IsUnicode(false);

            modelBuilder.Entity<Event>()
                .HasMany(e => e.Members)
                .WithMany(e => e.VisitedEvents)
                .Map(m => m.ToTable("Members"));

            modelBuilder.Entity<Person>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Person>()
                .HasMany(e => e.CreatedEvents)
                .WithOptional(e => e.Creator)
                .HasForeignKey(e => e.Creator_ID);
        }
    }
}
