﻿namespace EventManager.RPC.Tcp.Client.ViewForms
{
    partial class ViewForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tvEvents = new System.Windows.Forms.TreeView();
            this.btnAll = new System.Windows.Forms.Button();
            this.tbId = new System.Windows.Forms.TextBox();
            this.btnId = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.tbDepth = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // tvEvents
            // 
            this.tvEvents.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tvEvents.Location = new System.Drawing.Point(12, 41);
            this.tvEvents.Name = "tvEvents";
            this.tvEvents.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tvEvents.Size = new System.Drawing.Size(348, 215);
            this.tvEvents.TabIndex = 0;
            // 
            // btnAll
            // 
            this.btnAll.Location = new System.Drawing.Point(12, 12);
            this.btnAll.Name = "btnAll";
            this.btnAll.Size = new System.Drawing.Size(75, 23);
            this.btnAll.TabIndex = 1;
            this.btnAll.Text = "All";
            this.btnAll.UseVisualStyleBackColor = true;
            this.btnAll.Click += new System.EventHandler(this.btnAll_Click);
            // 
            // tbId
            // 
            this.tbId.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tbId.Location = new System.Drawing.Point(224, 13);
            this.tbId.Name = "tbId";
            this.tbId.Size = new System.Drawing.Size(55, 20);
            this.tbId.TabIndex = 2;
            // 
            // btnId
            // 
            this.btnId.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnId.Location = new System.Drawing.Point(285, 11);
            this.btnId.Name = "btnId";
            this.btnId.Size = new System.Drawing.Size(75, 23);
            this.btnId.TabIndex = 3;
            this.btnId.Text = "By Id";
            this.btnId.UseVisualStyleBackColor = true;
            this.btnId.Click += new System.EventHandler(this.btnId_Click);
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(133, 266);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Depth";
            // 
            // tbDepth
            // 
            this.tbDepth.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.tbDepth.Location = new System.Drawing.Point(175, 263);
            this.tbDepth.Name = "tbDepth";
            this.tbDepth.Size = new System.Drawing.Size(49, 20);
            this.tbDepth.TabIndex = 5;
            this.tbDepth.Text = "1";
            // 
            // EventsViewForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(372, 291);
            this.Controls.Add(this.tbDepth);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnId);
            this.Controls.Add(this.tbId);
            this.Controls.Add(this.btnAll);
            this.Controls.Add(this.tvEvents);
            this.MinimumSize = new System.Drawing.Size(304, 292);
            this.Name = "EventsViewForm";
            this.Text = "Events";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TreeView tvEvents;
        private System.Windows.Forms.Button btnAll;
        private System.Windows.Forms.TextBox tbId;
        private System.Windows.Forms.Button btnId;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbDepth;
    }
}