﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using EventManager.Model.DTO;
using EventManager.RPC.Tcp.Interfaces;

namespace EventManager.RPC.Tcp.Client.ViewForms
{
    public partial class ViewForm : Form
    {
        public enum EntityType
        {
            Event,
            Person,
            Category
        }

        private readonly EntityType _activeEntity;
        private readonly ICrudService _service;

        public ViewForm(ICrudService service, EntityType entityType)
        {
            _service = service;
            InitializeComponent();
            _activeEntity = entityType;
        }

        private void btnAll_Click(object sender, EventArgs e)
        {
            try
            {
                int depth = Convert.ToInt32(tbDepth.Text);

                tvEvents.Nodes.Clear();
                if (_activeEntity == EntityType.Event) AddEventsToTree(depth);
                else if (_activeEntity == EntityType.Category) AddCategoriesToTree(depth);
                else AddPersonsToTree(depth);
                tvEvents.Invalidate();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
            
        }

        private void AddEventsToTree(int depth)
        {
            List<EventDTO> source = Enumerable.ToList(_service.GetAll(new GetEntityInfo(typeof(EventDTO))));
            tvEvents.Nodes.AddRange(source.Visualize(depth));
        }

        private void AddPersonsToTree(int depth)
        {
            List<PersonDTO> source = Enumerable.ToList(_service.GetAll(new GetEntityInfo(typeof(PersonDTO))));
            tvEvents.Nodes.AddRange(source.Visualize(depth));
        }

        private void AddCategoriesToTree(int depth)
        {
            List<CategoryDTO> source = Enumerable.ToList(_service.GetAll(new GetEntityInfo(typeof(CategoryDTO))));
            tvEvents.Nodes.AddRange(source.Visualize(depth));
        }

        private void btnId_Click(object sender, EventArgs e)
        {
            try
            {
                int depth = Convert.ToInt32(tbDepth.Text);
                int id = Convert.ToInt32(tbId.Text);

                tvEvents.Nodes.Clear();
                if (_activeEntity == EntityType.Event) AddEventToTree(depth,id);
                else if (_activeEntity == EntityType.Category) AddCategoryToTree(depth,id);
                else AddPersonToTree(depth,id);
                tvEvents.Invalidate();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        private void AddEventToTree(int depth, int id)
        {
            var evt = (EventDTO)_service.Get(new GetEntityInfo(typeof(EventDTO), new object[]{id}));
            if (evt == null) MessageBox.Show($@"There is no event with ID = {id}");
            else tvEvents.Nodes.Add(evt.Visualize(depth));

        }

        private void AddPersonToTree(int depth, int id)
        {
            var evt = (PersonDTO) _service.Get(new GetEntityInfo(typeof(PersonDTO), new object[] {id}));
            if (evt == null) MessageBox.Show($@"There is no event with ID = {id}");
            else tvEvents.Nodes.Add(evt.Visualize(depth));

        }

        private void AddCategoryToTree(int depth, int id)
        {
            var evt = (CategoryDTO) _service.Get(new GetEntityInfo(typeof(CategoryDTO), new object[] {id}));
            if (evt == null) MessageBox.Show($@"There is no event with ID = {id}");
            else tvEvents.Nodes.Add(evt.Visualize(depth));

        }
    }
}
