﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using EventManager.RPC.Tcp.Interfaces;
using Newtonsoft.Json;

namespace EventManager.RPC.Tcp.Client
{
    public class CrudServiceClient: ICrudService
    {
        private readonly TcpClient _client;

        public CrudServiceClient(TcpClient client)
        {
            _client = client;
        }

        private static void SendJson(string json, NetworkStream stream)
        {
            byte[] message = Encoding.ASCII.GetBytes(json);
            stream.Write(BitConverter.GetBytes(message.Length), 0, 4);
            stream.Write(message, 0, message.Length);
        }

        private static string RecieveJson(NetworkStream stream)
        {
            byte[] size = new byte[4];
            stream.Read(size, 0, 4);
            byte[] response = new byte[BitConverter.ToInt32(size,0)];
            stream.Read(response, 0, BitConverter.ToInt32(size,0));
            return Encoding.ASCII.GetString(response);
        }

        public object Get(GetEntityInfo info)
        {
            string request = JsonConvert.SerializeObject(new Command("Get", info),
                new JsonSerializerSettings
                {
                    TypeNameHandling = TypeNameHandling.All,
                    Formatting = Formatting.Indented
                });

            SendJson(request, _client.GetStream());
            string response = RecieveJson(_client.GetStream());

            return JsonConvert.DeserializeObject(response, info.EntityType, new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.All
            });
        }

        public dynamic GetAll(GetEntityInfo info)
        {
            string request = JsonConvert.SerializeObject(new Command("GetAll", info),
                new JsonSerializerSettings
                {
                    TypeNameHandling = TypeNameHandling.All,
                    Formatting = Formatting.Indented
                });

            SendJson(request, _client.GetStream());
            string response = RecieveJson(_client.GetStream());

            return JsonConvert.DeserializeObject(response, typeof(List<>).MakeGenericType(info.EntityType), new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.All
            });
        }

        public void Add(object entity)
        {
            string request = JsonConvert.SerializeObject(new Command("Add", entity ),
                new JsonSerializerSettings
                {
                    TypeNameHandling = TypeNameHandling.All,
                    Formatting = Formatting.Indented
                });

            SendJson(request, _client.GetStream());
        }

        public void Update(object entity)
        {
            string request = JsonConvert.SerializeObject(new Command("Update", entity ),
                new JsonSerializerSettings
                {
                    TypeNameHandling = TypeNameHandling.All
                });

            SendJson(request, _client.GetStream());
        }

        public void Delete(object entity)
        {
            string request = JsonConvert.SerializeObject(new Command("Delete", entity),
                new JsonSerializerSettings
                {
                    TypeNameHandling = TypeNameHandling.All,
                    Formatting = Formatting.Indented
                });

            SendJson(request, _client.GetStream());
        }
    }
}
