﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using EventManager.Model.DTO;
using EventManager.RPC.Tcp.Interfaces;

namespace EventManager.RPC.Tcp.Client.UpdateForms
{
    public partial class EventUpdateForm : Form
    {
        private EventDTO _activeEvent;
        private readonly ICrudService _service;

        public EventUpdateForm(ICrudService service)
        {
            _service = service;

            InitializeComponent();

            cbCategory.DataSource = Enumerable.ToList(_service.GetAll(new GetEntityInfo(typeof(CategoryDTO))));
            cbCreator.DataSource = Enumerable.ToList(_service.GetAll(new GetEntityInfo(typeof(PersonDTO)))); 
            clbMembers.DataSource = Enumerable.ToList(_service.GetAll(new GetEntityInfo(typeof(PersonDTO)))); 

            Reset();
        }

        private void Reset()
        {
            chNoCreator.Checked = true;
            chNoCategory.Checked = true;
            cbCreator.SelectedIndex = 0;
            cbCategory.SelectedIndex = 0;
            tbTitle.Text = "";
            tbTitle.Enabled = false;

            clbMembers.ClearSelected();

            cbCategory.Enabled = false;
            cbCreator.Enabled = false;
            clbMembers.Enabled = false;
            chNoCreator.Enabled = false;
            chNoCategory.Enabled = false;

            btnDelete.Enabled = false;
            btnUpdate.Enabled = false;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(tbTitle.Text) ||
                cbCategory.SelectedIndex == -1 ||
                cbCreator.SelectedIndex == -1)
                MessageBox.Show(@"Entered data is not full", @"Enter data!",
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            else
            {
                _activeEvent.Title = tbTitle.Text;
                _activeEvent.Category_ID = chNoCategory.Checked
                    ? null
                    : (cbCategory.DataSource as List<CategoryDTO>)?[cbCategory.SelectedIndex]?.ID;
                _activeEvent.Creator_ID = chNoCreator.Checked
                    ? null
                    : (cbCreator.DataSource as List<PersonDTO>)?[cbCreator.SelectedIndex]?.ID;
                
                _activeEvent.Members.Clear();
                foreach (var i in clbMembers.CheckedIndices)
                {
                    _activeEvent.Members.Add((clbMembers.DataSource as List<PersonDTO>)?[(int)i]);
                }

                _service.Update(_activeEvent);
                
                Close();
            }
        }

        private void chNoCreator_CheckedChanged(object sender, EventArgs e)
        {
            cbCreator.Enabled = !((CheckBox)sender).Checked;
        }

        private void chNoCategory_CheckedChanged(object sender, EventArgs e)
        {
            cbCategory.Enabled = !((CheckBox)sender).Checked;
        }

        private void btnChoose_Click(object sender, EventArgs e)
        {
            try
            {
                _activeEvent =
                    (EventDTO)
                    _service.Get(new GetEntityInfo(typeof(EventDTO), new object[] {Convert.ToInt32(tbId.Text)}));

                if (_activeEvent != null)
                {
                    cbCategory.Enabled = _activeEvent.Category_ID != null;
                    cbCreator.Enabled = _activeEvent.Creator_ID != null;
                    clbMembers.Enabled = true;
                    chNoCreator.Enabled = true;
                    chNoCategory.Enabled = true;
                    tbTitle.Enabled = true;

                    tbTitle.Text = _activeEvent.Title;

                    chNoCreator.Checked = _activeEvent.Creator_ID == null;
                    chNoCategory.Checked = _activeEvent.Category_ID == null;

                    for (var i = 0; i < clbMembers.Items.Count; i++)
                    {
                        if (_activeEvent.Members.Any(m => m.ID == ((PersonDTO) clbMembers.Items[i]).ID))
                            clbMembers.SetItemChecked(i, true);
                    }

                    if (_activeEvent.Category_ID != null) cbCategory.Text = _activeEvent.Category.ToString();
                    if (_activeEvent.Creator_ID != null) cbCreator.Text = _activeEvent.Creator.ToString();

                    btnUpdate.Enabled = true;
                    btnDelete.Enabled = true;
                }
                else MessageBox.Show($@"There is no event with ID = {Convert.ToInt32(tbId.Text)}");

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void tbId_TextChanged(object sender, EventArgs e)
        {
            Reset();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            _service.Delete(_activeEvent);

            Close();
        }

        private void EventUpdateForm_Load(object sender, EventArgs e)
        {

        }
    }
}
