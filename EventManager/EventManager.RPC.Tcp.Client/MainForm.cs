﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Windows.Forms;
using EventManager.RPC.Tcp.Client.CreateForms;
using EventManager.RPC.Tcp.Client.UpdateForms;
using EventManager.RPC.Tcp.Client.ViewForms;
using EventManager.RPC.Tcp.Interfaces;

namespace EventManager.RPC.Tcp.Client
{
    public partial class MainForm : Form
    {
        private ICrudService _service;
        private TcpClient _client;

        public MainForm()
        {
            InitializeComponent();
        }

        private void btnCreateEvent_Click(object sender, EventArgs e)
        {
            CreateEventFrom form = new CreateEventFrom(_service);
            form.ShowDialog();
        }

        private void btnCreatePerson_Click(object sender, EventArgs e)
        {
            CreatePersonForm form = new CreatePersonForm(_service);
            form.ShowDialog();
        }

        private void btnCreateCategory_Click(object sender, EventArgs e)
        {
            CreateCategoryForm form = new CreateCategoryForm(_service) {Text = @"Category"};
            form.ShowDialog();
        }

        private void btnViewEvents_Click(object sender, EventArgs e)
        {
            ViewForm form = new ViewForm(_service, ViewForm.EntityType.Event) {Text = @"Events"};
            form.ShowDialog();
        }

        private void btnViewPerson_Click(object sender, EventArgs e)
        {
            ViewForm form = new ViewForm(_service, ViewForm.EntityType.Person) {Text = @"Person"};
            form.ShowDialog();
        }

        private void btnUpdateEvent_Click(object sender, EventArgs e)
        {
            EventUpdateForm form = new EventUpdateForm(_service) { Text = @"Person" };
            form.ShowDialog();
        }

        private void btnUpdateCategory_Click(object sender, EventArgs e)
        {
            CategoryUpdateForm form = new CategoryUpdateForm(_service);
            form.ShowDialog();
        }

        private void btnViewCategories_Click(object sender, EventArgs e)
        {
            ViewForm form = new ViewForm(_service, ViewForm.EntityType.Category) { Text = @"Category" };
            form.ShowDialog();
        }

        private void btnUpdatePerson_Click(object sender, EventArgs e)
        {
            PersonUpdateForm form = new PersonUpdateForm(_service);
            form.ShowDialog();
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            _client = new TcpClient();
            try
            {
                _client.ReceiveBufferSize = 104857600;
                _client.SendBufferSize = 104857600;
                _client.Connect(IPAddress.Parse(textBox1.Text), 15001);
                _service = new CrudServiceClient(_client);
                btnConnect.Enabled = false;
                btnCreateCategory.Enabled = true;
                btnCreateEvent.Enabled = true;
                btnCreatePerson.Enabled = true;
                btnUpdateCategory.Enabled = true;
                btnUpdateEvent.Enabled = true;
                btnUpdatePerson.Enabled = true;
                btnViewCategories.Enabled = true;
                btnViewEvents.Enabled = true;
                btnViewPersons.Enabled = true;
            }
            catch (Exception ex)
            {
                _client.Close();
                _service = null;
                MessageBox.Show(ex.Message);
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            _client?.Close();
            _service = null;
            btnConnect.Enabled = true;
            btnCreateCategory.Enabled = false;
            btnCreateEvent.Enabled = false;
            btnCreatePerson.Enabled = false;
            btnUpdateCategory.Enabled = false;
            btnUpdateEvent.Enabled = false;
            btnUpdatePerson.Enabled = false;
            btnViewCategories.Enabled = false;
            btnViewEvents.Enabled = false;
            btnViewPersons.Enabled = false;
        }
    }
}
