﻿using System;
using System.Windows.Forms;
using EventManager.Model.DTO;
using EventManager.RPC.Tcp.Interfaces;

namespace EventManager.RPC.Tcp.Client.CreateForms
{
    public partial class CreateCategoryForm : Form
    {
        private readonly ICrudService _service;

        public CreateCategoryForm(ICrudService service)
        {
            _service = service;
            InitializeComponent();
        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(tbName.Text))
                MessageBox.Show(@"Entered data is not full", @"Enter data!",
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            else
            {
                var newCategory = new CategoryDTO() { Name = tbName.Text };

                _service.Add(newCategory);

                Close();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
