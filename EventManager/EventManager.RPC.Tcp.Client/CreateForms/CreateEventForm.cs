﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using EventManager.Model.DTO;
using EventManager.RPC.Tcp.Interfaces;

namespace EventManager.RPC.Tcp.Client.CreateForms
{
    public partial class CreateEventFrom : Form
    {
        private readonly ICrudService _service;

        public CreateEventFrom(ICrudService service)
        {
            _service = service;

            InitializeComponent();

            cbCategory.DataSource = Enumerable.ToList(_service.GetAll(new GetEntityInfo(typeof(CategoryDTO))));
            cbCreator.DataSource = Enumerable.ToList(_service.GetAll(new GetEntityInfo(typeof(PersonDTO)))); ;
            clbMembers.DataSource = Enumerable.ToList(_service.GetAll(new GetEntityInfo(typeof(PersonDTO)))); ;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(tbTitle.Text) ||
                cbCategory.SelectedIndex == -1 ||
                cbCreator.SelectedIndex == -1)
                MessageBox.Show(@"Entered data is not full", @"Enter data!",
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            else
            {
                var newEvent = new EventDTO()
                {
                    Title = tbTitle.Text,
                    Category_ID = chNoCategory.Checked
                        ? null
                        : (cbCategory.DataSource as List<CategoryDTO>)?[cbCategory.SelectedIndex]?.ID,
                    Creator_ID = chNoCreator.Checked
                        ? null
                        : (cbCreator.DataSource as List<PersonDTO>)?[cbCreator.SelectedIndex]?.ID
                };
                foreach (var i in clbMembers.CheckedIndices)
                {
                    newEvent.Members.Add((clbMembers.DataSource as List<PersonDTO>)?[(int)i]);
                }

                _service.Add(newEvent);
                
                Close();
            }
        }

        private void chNoCreator_CheckedChanged(object sender, EventArgs e)
        {
            cbCreator.Enabled = !((CheckBox) sender).Checked;
        }

        private void chNoCategory_CheckedChanged(object sender, EventArgs e)
        {
            cbCategory.Enabled = !((CheckBox)sender).Checked;
        }
    }
}
