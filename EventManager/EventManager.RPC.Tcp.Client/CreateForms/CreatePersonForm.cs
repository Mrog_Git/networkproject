﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using EventManager.Model.DTO;
using EventManager.RPC.Tcp.Interfaces;

namespace EventManager.RPC.Tcp.Client.CreateForms
{
    public partial class CreatePersonForm : Form
    {

        private readonly ICrudService _service; 

        public CreatePersonForm(ICrudService service)
        {
            InitializeComponent();

            _service = service;

            clbEvents.DataSource = Enumerable.ToList(service.GetAll(new GetEntityInfo(typeof(EventDTO))));
        }

        private void btnCancel_Click(object sender, System.EventArgs e)
        {
            Close();
        }

        private void btnCreate_Click(object sender, System.EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(tbName.Text))
                MessageBox.Show(@"Entered data is not full", @"Enter data!",
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            else
            {
                var newPerson = new PersonDTO() {Name = tbName.Text};
                foreach (var i in clbEvents.CheckedIndices)
                {
                    newPerson.VisitedEvents.Add((clbEvents.DataSource as List<EventDTO>)?[(int)i]);
                }

                _service.Add(newPerson);

                Close();
            }
        }
    }
}
