﻿namespace EventManager.RPC.Tcp.Client
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCreateEvent = new System.Windows.Forms.Button();
            this.btnCreatePerson = new System.Windows.Forms.Button();
            this.btnCreateCategory = new System.Windows.Forms.Button();
            this.btnViewEvents = new System.Windows.Forms.Button();
            this.btnViewPersons = new System.Windows.Forms.Button();
            this.btnViewCategories = new System.Windows.Forms.Button();
            this.btnUpdateEvent = new System.Windows.Forms.Button();
            this.btnUpdateCategory = new System.Windows.Forms.Button();
            this.btnUpdatePerson = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.btnConnect = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnCreateEvent
            // 
            this.btnCreateEvent.Enabled = false;
            this.btnCreateEvent.Location = new System.Drawing.Point(23, 76);
            this.btnCreateEvent.Name = "btnCreateEvent";
            this.btnCreateEvent.Size = new System.Drawing.Size(101, 23);
            this.btnCreateEvent.TabIndex = 0;
            this.btnCreateEvent.Text = "Create Event";
            this.btnCreateEvent.UseVisualStyleBackColor = true;
            this.btnCreateEvent.Click += new System.EventHandler(this.btnCreateEvent_Click);
            // 
            // btnCreatePerson
            // 
            this.btnCreatePerson.Enabled = false;
            this.btnCreatePerson.Location = new System.Drawing.Point(23, 105);
            this.btnCreatePerson.Name = "btnCreatePerson";
            this.btnCreatePerson.Size = new System.Drawing.Size(101, 23);
            this.btnCreatePerson.TabIndex = 1;
            this.btnCreatePerson.Text = "Create Person";
            this.btnCreatePerson.UseVisualStyleBackColor = true;
            this.btnCreatePerson.Click += new System.EventHandler(this.btnCreatePerson_Click);
            // 
            // btnCreateCategory
            // 
            this.btnCreateCategory.Enabled = false;
            this.btnCreateCategory.Location = new System.Drawing.Point(23, 134);
            this.btnCreateCategory.Name = "btnCreateCategory";
            this.btnCreateCategory.Size = new System.Drawing.Size(101, 23);
            this.btnCreateCategory.TabIndex = 2;
            this.btnCreateCategory.Text = "Create Category";
            this.btnCreateCategory.UseVisualStyleBackColor = true;
            this.btnCreateCategory.Click += new System.EventHandler(this.btnCreateCategory_Click);
            // 
            // btnViewEvents
            // 
            this.btnViewEvents.Enabled = false;
            this.btnViewEvents.Location = new System.Drawing.Point(130, 76);
            this.btnViewEvents.Name = "btnViewEvents";
            this.btnViewEvents.Size = new System.Drawing.Size(105, 23);
            this.btnViewEvents.TabIndex = 3;
            this.btnViewEvents.Text = "View Events";
            this.btnViewEvents.UseVisualStyleBackColor = true;
            this.btnViewEvents.Click += new System.EventHandler(this.btnViewEvents_Click);
            // 
            // btnViewPersons
            // 
            this.btnViewPersons.Enabled = false;
            this.btnViewPersons.Location = new System.Drawing.Point(130, 105);
            this.btnViewPersons.Name = "btnViewPersons";
            this.btnViewPersons.Size = new System.Drawing.Size(105, 23);
            this.btnViewPersons.TabIndex = 4;
            this.btnViewPersons.Text = "View Persons";
            this.btnViewPersons.UseVisualStyleBackColor = true;
            this.btnViewPersons.Click += new System.EventHandler(this.btnViewPerson_Click);
            // 
            // btnViewCategories
            // 
            this.btnViewCategories.Enabled = false;
            this.btnViewCategories.Location = new System.Drawing.Point(130, 134);
            this.btnViewCategories.Name = "btnViewCategories";
            this.btnViewCategories.Size = new System.Drawing.Size(105, 23);
            this.btnViewCategories.TabIndex = 5;
            this.btnViewCategories.Text = "View Categories";
            this.btnViewCategories.UseVisualStyleBackColor = true;
            this.btnViewCategories.Click += new System.EventHandler(this.btnViewCategories_Click);
            // 
            // btnUpdateEvent
            // 
            this.btnUpdateEvent.Enabled = false;
            this.btnUpdateEvent.Location = new System.Drawing.Point(241, 76);
            this.btnUpdateEvent.Name = "btnUpdateEvent";
            this.btnUpdateEvent.Size = new System.Drawing.Size(105, 23);
            this.btnUpdateEvent.TabIndex = 6;
            this.btnUpdateEvent.Text = "Update Event";
            this.btnUpdateEvent.UseVisualStyleBackColor = true;
            this.btnUpdateEvent.Click += new System.EventHandler(this.btnUpdateEvent_Click);
            // 
            // btnUpdateCategory
            // 
            this.btnUpdateCategory.Enabled = false;
            this.btnUpdateCategory.Location = new System.Drawing.Point(241, 134);
            this.btnUpdateCategory.Name = "btnUpdateCategory";
            this.btnUpdateCategory.Size = new System.Drawing.Size(105, 23);
            this.btnUpdateCategory.TabIndex = 7;
            this.btnUpdateCategory.Text = "Update Category";
            this.btnUpdateCategory.UseVisualStyleBackColor = true;
            this.btnUpdateCategory.Click += new System.EventHandler(this.btnUpdateCategory_Click);
            // 
            // btnUpdatePerson
            // 
            this.btnUpdatePerson.Enabled = false;
            this.btnUpdatePerson.Location = new System.Drawing.Point(241, 105);
            this.btnUpdatePerson.Name = "btnUpdatePerson";
            this.btnUpdatePerson.Size = new System.Drawing.Size(105, 23);
            this.btnUpdatePerson.TabIndex = 8;
            this.btnUpdatePerson.Text = "Update Person";
            this.btnUpdatePerson.UseVisualStyleBackColor = true;
            this.btnUpdatePerson.Click += new System.EventHandler(this.btnUpdatePerson_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(110, 24);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 9;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // btnConnect
            // 
            this.btnConnect.Location = new System.Drawing.Point(221, 22);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(75, 23);
            this.btnConnect.TabIndex = 10;
            this.btnConnect.Text = "Connect";
            this.btnConnect.UseVisualStyleBackColor = true;
            this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(87, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(23, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "IP: ";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(376, 178);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnConnect);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.btnUpdatePerson);
            this.Controls.Add(this.btnUpdateCategory);
            this.Controls.Add(this.btnUpdateEvent);
            this.Controls.Add(this.btnViewCategories);
            this.Controls.Add(this.btnViewPersons);
            this.Controls.Add(this.btnViewEvents);
            this.Controls.Add(this.btnCreateCategory);
            this.Controls.Add(this.btnCreatePerson);
            this.Controls.Add(this.btnCreateEvent);
            this.Name = "MainForm";
            this.Text = "Event Manager";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCreateEvent;
        private System.Windows.Forms.Button btnCreatePerson;
        private System.Windows.Forms.Button btnCreateCategory;
        private System.Windows.Forms.Button btnViewEvents;
        private System.Windows.Forms.Button btnViewPersons;
        private System.Windows.Forms.Button btnViewCategories;
        private System.Windows.Forms.Button btnUpdateEvent;
        private System.Windows.Forms.Button btnUpdateCategory;
        private System.Windows.Forms.Button btnUpdatePerson;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button btnConnect;
        private System.Windows.Forms.Label label1;
    }
}

