﻿using EventManager.Model.DTO;
using EventManager.Model.EF.Repository;
using EventManager.RPC.Tcp.Interfaces;

namespace EventManager.RPC.Tcp.Server
{
    public class CrudService: ICrudService
    {
        private readonly DynamicCrudRepository _repo = new DynamicCrudRepository();

        public object Get(GetEntityInfo info)
        {
            return DTOConverter.Convert(_repo.Get(DTOConverter.GetRelatedType(info.EntityType), info.KeyParams));
        }

        public dynamic GetAll(GetEntityInfo info)
        {
            return DTOConverter.Convert(_repo.GetAll(DTOConverter.GetRelatedType(info.EntityType)));
        }

        public void Add(object entity)
        {
            _repo.Add(DTOConverter.Convert(entity));
        }

        public void Update(object entity)
        {
            _repo.Update(DTOConverter.Convert(entity));
        }

        public void Delete(object entity)
        {
            _repo.Delete(DTOConverter.Convert(entity));
        }
    }
}
