﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using EventManager.Model.DTO;
using EventManager.RPC.Tcp.Interfaces;
using Newtonsoft.Json;

namespace EventManager.RPC.Tcp.Server
{
    internal class Client : IDisposable
    {
        private readonly TcpClient _client;

        public Client(TcpClient client)
        {
            _client = client;
        }

        public void Dispose()
        {
            _client.Close();
        }

        private async Task<Command> ReadFromStreamAsync()
        {
            byte[] size = new byte[4];
            await _client.GetStream().ReadAsync(size, 0, 4);
            byte[] response = new byte[BitConverter.ToInt32(size, 0)];
            await _client.GetStream().ReadAsync(response, 0, BitConverter.ToInt32(size, 0));
            return JsonConvert.DeserializeObject<Command>(Encoding.ASCII.GetString(response), new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.All
            });
        }

        private async Task WriteToStreamAsync(object obj)
        {
            string json = JsonConvert.SerializeObject(obj,
                new JsonSerializerSettings
                {
                    TypeNameHandling = TypeNameHandling.All,
                    Formatting = Formatting.Indented
                });

            byte[] message = Encoding.ASCII.GetBytes(json);
            await _client.GetStream().WriteAsync(BitConverter.GetBytes(message.Length), 0, 4);
            await _client.GetStream().WriteAsync(message, 0, message.Length);
        }

        public async Task ProcessAsync()
        {
            while (_client.Connected)
            {
                var command = await ReadFromStreamAsync();
                if (typeof(CrudService).GetMethod(command.MethodName).ReturnType != typeof(void))
                {
                    var result = typeof(CrudService).GetMethod(command.MethodName)?.Invoke(new CrudService(), new[] { command.Param });
                    await WriteToStreamAsync(result);
                }
                else typeof(CrudService).GetMethod(command.MethodName)?.Invoke(new CrudService(), new[] { command.Param });
            }
        }
    }

    internal static class Program
    {
        private static readonly HashSet<Task> ActiveClientTasks = new HashSet<Task>();

        private static void Main(string[] args)
        {
            DTOConverter.Initialize();

            var listener = new TcpListener(IPAddress.Any, 15001);
       
            listener.Start();
            
            Console.WriteLine("Press ESC to stop.");
            do
            {
                while (!Console.KeyAvailable)
                {
                    var tcpClient = listener.AcceptTcpClient();
                    tcpClient.ReceiveBufferSize = 104857600;
                    tcpClient.SendBufferSize = 104857600;
                    ProcessClient(tcpClient);
                }
            } while (Console.ReadKey(true).Key != ConsoleKey.Escape);

            listener.Stop();
            Task.WaitAll(ActiveClientTasks.ToArray());
        }

        private static async void ProcessClient(TcpClient tcpClient)
        {
            using (var client = new Client(tcpClient))
            {
                Task task = null;
                try
                {
                    task = client.ProcessAsync();
                    ActiveClientTasks.Add(task);
                    await task;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
                finally
                {
                    if (task != null)
                        ActiveClientTasks.Remove(task);
                }
            }
        }
    }
}
