﻿using System.Collections.Generic;
using WebApi.Hal;

namespace EventManager.WebApi.HATEOS.Resources
{
    public class PersonListRepresentation : SimpleListRepresentation<PersonRepresentation>
    {
        public PersonListRepresentation(IList<PersonRepresentation> people) : base(people)
        {
        }

        protected override void CreateHypermedia()
        {
            Href = LinkTemplates.People.GeneralPeople.Href;

            Links.Add(new Link { Href = Href, Rel = "self" });
        }
    }
}