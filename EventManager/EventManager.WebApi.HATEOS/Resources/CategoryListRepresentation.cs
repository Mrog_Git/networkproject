﻿using System.Collections.Generic;
using WebApi.Hal;

namespace EventManager.WebApi.HATEOS.Resources
{
    public class CategoryListRepresentation : SimpleListRepresentation<CategoryRepresentation>
    {
        public CategoryListRepresentation(IList<CategoryRepresentation> categories) : base(categories)
        {
        }

        protected override void CreateHypermedia()
        {
            Href = LinkTemplates.Categories.GeneralCategories.Href;

            Links.Add(new Link { Href = Href, Rel = "self" });
        }
    }
}