﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EventManager.Model.EF.Entities;

namespace EventManager.WebApi.HATEOS.Resources
{
    public class MemberRepresentation: PersonRepresentation
    {
        public MemberRepresentation(Person person, int depth, int curDepth = 0): base(person,depth,curDepth)
        {
                
        }

        public override string Rel
        {
            get { return "member"; }
            set { }
        }
    }
}