﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EventManager.Model.EF.Entities;

namespace EventManager.WebApi.HATEOS.Resources
{
    public class VisitedEventRepresentation: EventRepresentation
    {
        public VisitedEventRepresentation(Event @event, int depth, int curDepth = 0) : base(@event, depth, curDepth)
        {

        }

        public override string Rel
        {
            get { return "visited_event"; }
            set { }
        }
    }
}