﻿using System.Collections.Generic;
using EventManager.Model.EF.Entities;
using WebApi.Hal;

namespace EventManager.WebApi.HATEOS.Resources
{
    public class EventRepresentation: Representation
    {

        public EventRepresentation(Event @event, int depth, int curDepth = 0)
        {
            ID = @event.ID;
            Title = @event.Title;
            Creator_ID = @event.Creator_ID;
            Category_ID = @event.Category_ID;
            Members = new List<PersonRepresentation>();
            if (curDepth == depth) return;
            if (@event.Creator != null) Creator = new PersonRepresentation(@event.Creator, depth, curDepth + 1);
            if (@event.Category != null) Category = new CategoryRepresentation(@event.Category, depth, curDepth + 1);
            if (@event.Members == null) return;
            foreach (var member in @event.Members)
            {
                Members.Add(new MemberRepresentation(member, depth, curDepth + 1));
            }
        }

        public EventRepresentation()
        {
            Members = new List<PersonRepresentation>();
        }

        public int ID { get; set; }

        public string Title { get; set; }

        public int? Creator_ID { get; set; }

        public int? Category_ID { get; set; }

        public CategoryRepresentation Category { get; set; }

        public PersonRepresentation Creator { get; set; }

        public List<PersonRepresentation> Members { get; set; }

        public override string ToString()
        {
            return Title + $" (ID = {ID})";
        }

        public override string Rel
        {
            get { return LinkTemplates.Events.Event.Rel; }
            set { }
        }

        public override string Href
        {
            get { return LinkTemplates.Events.Event.CreateLink(new { id = ID }).Href; }
            set { }
        }

        protected override void CreateHypermedia()
        {
            if (Category != null)
                Links.Add(LinkTemplates.Categories.Category.CreateLink(new { id = Category.ID }));
            if (Creator != null)
                Links.Add(LinkTemplates.People.Person.CreateLink(new { id = Creator.ID }));
            if (Members != null)
                foreach (var mem in Members)
                    Links.Add(LinkTemplates.People.Person.CreateLink(new { id = mem.ID }));
        }

        public Event CreateEntity()
        {
            var @event = new Event()
            {
                ID = ID,
                Title = Title,
                Category_ID = Category_ID,
                Creator_ID = Creator_ID
            };
            if (Creator != null) @event.Creator = Creator.CreateEntity();
            if (Category != null) @event.Category = Category.CreateEntity();
            if (Members == null) return @event;
            foreach (var member in Members)
            {
                @event.Members.Add(member.CreateEntity());
            }

            return @event;
        }
    }
}