﻿using System.Collections.Generic;
using EventManager.Model.EF.Entities;
using WebApi.Hal;

namespace EventManager.WebApi.HATEOS.Resources
{
    public class CategoryRepresentation: Representation
    {
        public CategoryRepresentation()
        {
            Events = new List<EventRepresentation>();
        }

        public CategoryRepresentation(Category category, int depth, int curDepth = 0)
        {
            ID = category.ID;
            Name = category.Name;
            Events = new List<EventRepresentation>();
            if (curDepth != depth && category.Events != null)
                foreach (var @event in category.Events)
                {
                    Events.Add(new EventRepresentation(@event, depth, curDepth + 1));
                }
        }

        public int ID { get; set; }

        public string Name { get; set; }

        public List<EventRepresentation> Events { get; set; }

        public override string ToString()
        {
            return Name + $" (ID = {ID})";
        }

        public override string Rel
        {
            get { return LinkTemplates.Categories.Category.Rel; }
            set { }
        }

        public override string Href
        {
            get { return LinkTemplates.Categories.Category.CreateLink(new { id = ID }).Href; }
            set { }
        }

        protected override void CreateHypermedia()
        {
            if (Events != null)
                foreach (var evt in Events)
                    Links.Add(LinkTemplates.Events.Event.CreateLink(new { id = evt.ID }));
        }

        public Category CreateEntity()
        {
            var category = new Category()
            {
                ID = ID,
                Name = Name
            };
            
            if (Events == null) return category;
            foreach (var @event in Events)
            {
                category.Events.Add(@event.CreateEntity());
            }
            return category;
        }
    }
}