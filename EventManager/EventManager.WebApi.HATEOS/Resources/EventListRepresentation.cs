﻿using System.Collections.Generic;
using WebApi.Hal;

namespace EventManager.WebApi.HATEOS.Resources
{
    public class EventListRepresentation : SimpleListRepresentation<EventRepresentation>
    {
        public EventListRepresentation(IList<EventRepresentation> events) : base(events)
        {
        }

        protected override void CreateHypermedia()
        {
            Href = LinkTemplates.Events.GeneralEvents.Href;

            Links.Add(new Link { Href = Href, Rel = "self" });
        }
    }
}