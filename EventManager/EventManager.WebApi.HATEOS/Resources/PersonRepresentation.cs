﻿using System.Collections.Generic;
using EventManager.Model.EF.Entities;
using WebApi.Hal;

namespace EventManager.WebApi.HATEOS.Resources
{
    public class PersonRepresentation: Representation
    {
        public PersonRepresentation(Person person, int depth, int curDepth = 0)
        {
            ID = person.ID;
            Name = person.Name;
            CreatedEvents = new List<EventRepresentation>();
            VisitedEvents = new List<EventRepresentation>();
            if (curDepth != depth)
            {
                if (person.CreatedEvents == null) return;
                foreach (var @event in person.CreatedEvents)
                {
                    CreatedEvents.Add(new CreatedEventRepresentation(@event, depth, curDepth + 1));
                }
                if (person.VisitedEvents == null) return;
                foreach (var @event in person.VisitedEvents)
                {
                    VisitedEvents.Add(new VisitedEventRepresentation(@event, depth, curDepth + 1));
                }
            }
        }

        public PersonRepresentation()
        {
            CreatedEvents = new List<EventRepresentation>();
            VisitedEvents = new List<EventRepresentation>();
        }

        public int ID { get; set; }

        public string Name { get; set; }

        public List<EventRepresentation> CreatedEvents { get; set; }

        public List<EventRepresentation> VisitedEvents { get; set; }

        public override string ToString()
        {
            return Name + $" (ID = {ID})";
        }

        public override string Rel
        {
            get { return LinkTemplates.People.Person.Rel; }
            set { }
        }

        public override string Href
        {
            get { return LinkTemplates.People.Person.CreateLink(new { id = ID }).Href; }
            set { }
        }

        protected override void CreateHypermedia()
        {
            if (CreatedEvents != null)
                foreach (var evt in CreatedEvents)
                    Links.Add(LinkTemplates.Events.Event.CreateLink(new { id = evt.ID }));

            if (VisitedEvents != null)
                foreach (var evt in VisitedEvents)
                    Links.Add(LinkTemplates.Events.Event.CreateLink(new { id = evt.ID }));
        }

        public Person CreateEntity()
        {
            var person = new Person()
            {
                ID = ID,
                Name = Name
            };
            if (VisitedEvents == null) return person;
            foreach (var @event in VisitedEvents)
            {
                person.VisitedEvents.Add(@event.CreateEntity());
            }
            if (CreatedEvents == null) return person;
            foreach (var @event in CreatedEvents)
            {
                person.VisitedEvents.Add(@event.CreateEntity());
            }

            return person;
        }
    }
}