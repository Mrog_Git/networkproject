﻿using WebApi.Hal;

namespace EventManager.WebApi.HATEOS.Resources
{
    public class FunctionalityRepresentation: Representation
    {
        public override string Rel
        {
            get { return LinkTemplates.Functionality.GetFunctionality.Rel; }
            set { }
        }

        public override string Href
        {
            get { return LinkTemplates.Functionality.GetFunctionality.CreateLink().Href; }
            set { }
        }

        protected override void CreateHypermedia()
        {
            var curiesUser = new CuriesLink("people", "/documentation/people/{#rel}");

            Links.Add(curiesUser.CreateLink("create", LinkTemplates.People.GeneralPeople.Href));
            Links.Add(curiesUser.CreateLink("update", LinkTemplates.People.Person.Href));
            Links.Add(curiesUser.CreateLink("retrieve", LinkTemplates.People.Person.Href));
            Links.Add(curiesUser.CreateLink("delete", LinkTemplates.People.Person.Href));
            Links.Add(curiesUser.CreateLink("list", LinkTemplates.People.GeneralPeople.Href));


            var curiesReview = new CuriesLink("events", "/documentation/events/{#rel}");

            Links.Add(curiesReview.CreateLink("create", LinkTemplates.Events.GeneralEvents.Href));
            Links.Add(curiesReview.CreateLink("update", LinkTemplates.Events.Event.Href));
            Links.Add(curiesReview.CreateLink("retrieve", LinkTemplates.Events.Event.Href));
            Links.Add(curiesReview.CreateLink("delete", LinkTemplates.Events.Event.Href));
            Links.Add(curiesReview.CreateLink("list", LinkTemplates.Events.GeneralEvents.Href));

            var curiesCategory = new CuriesLink("categories", "/documentation/categories/{#rel}");

            Links.Add(curiesCategory.CreateLink("create", LinkTemplates.Categories.GeneralCategories.Href));
            Links.Add(curiesCategory.CreateLink("update", LinkTemplates.Categories.Category.Href));
            Links.Add(curiesCategory.CreateLink("retrieve", LinkTemplates.Categories.Category.Href));
            Links.Add(curiesCategory.CreateLink("delete", LinkTemplates.Categories.Category.Href));
            Links.Add(curiesCategory.CreateLink("list", LinkTemplates.Categories.GeneralCategories.Href));
        }
    }
}