﻿using WebApi.Hal;

namespace EventManager.WebApi.HATEOS
{
    public static class LinkTemplates
    {
        public static class Functionality
        {
            public static Link GetFunctionality => new Link("functionality", "/functionality");
        }

        public static class Events
        {
            public static Link Event { get { return new Link("event", "/events/{id}"); } }

            public static Link GeneralEvents { get { return new Link("events", "/events"); } }
        }

        public static class People
        {
            public static Link Person { get { return new Link("person", "/human/{id}"); } }

            public static Link GeneralPeople { get { return new Link("people", "/human"); } }
        }

        public static class Categories
        {
            public static Link Category { get { return new Link("category", "/categories/{id}"); } }

            public static Link GeneralCategories { get { return new Link("category", "/categories"); } }
        }
    }
}