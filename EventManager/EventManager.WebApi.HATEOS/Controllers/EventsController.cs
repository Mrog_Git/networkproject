﻿using System;
using System.Linq;
using System.Web.Http;
using EventManager.Model.EF.Entities;
using EventManager.Model.EF.Repository;
using EventManager.WebApi.HATEOS.Resources;

namespace EventManager.WebApi.HATEOS.Controllers
{
    public class EventsController : ApiController
    {
        private readonly CrudRepository<Event> _repository = new CrudRepository<Event>();
        private readonly int MaxDepth;

        public EventsController()
        {
            MaxDepth = 4;
        }


        public IHttpActionResult Get()
        {
            var list = _repository.GetAll().Select(c => new EventRepresentation(c, MaxDepth)).ToList();
            return Ok(new EventListRepresentation(list));
        }

        public IHttpActionResult Get(int id)
        {
            var e = _repository.Get(id);
            if (e != null) return Ok(new EventRepresentation(e, MaxDepth));
            return NotFound();
        }

        public IHttpActionResult Post(EventRepresentation evt)
        {
            try
            {
                _repository.Add(evt.CreateEntity());
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        public IHttpActionResult Put(int id, EventRepresentation evt)
        {
            if (id != evt.ID) return BadRequest();

            try
            {
                _repository.Update(evt.CreateEntity());
                return Ok();
            }
            catch (Exception e)
            {
                if (_repository.Get(id) == null) return NotFound();
                return BadRequest(e.Message);
            }
        }

        public IHttpActionResult Delete(int id)
        {
            var evt = _repository.Get(id);
            if (evt == null) return NotFound();
            try
            {
                _repository.Delete(evt);
                return Ok();
            }
            catch (Exception e)
            {
                if (_repository.Get(id) == null) return NotFound();
                return BadRequest(e.Message);
            }
        }
    }
}