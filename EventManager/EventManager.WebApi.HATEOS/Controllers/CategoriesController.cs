﻿using System;
using System.Linq;
using System.Web.Http;
using EventManager.Model.EF.Entities;
using EventManager.Model.EF.Repository;
using EventManager.WebApi.HATEOS.Resources;

namespace EventManager.WebApi.HATEOS.Controllers
{
    public class CategoriesController : ApiController
    {
        private readonly CrudRepository<Category> _repository = new CrudRepository<Category>();
        private readonly int MaxDepth;

        public CategoriesController()
        {
            MaxDepth = 4;
        }

        //[CallFrequencyLimit(Name = "CategoryGetAll")]
        public IHttpActionResult Get()
        {
            var list = _repository.GetAll().Select(c => new CategoryRepresentation(c, MaxDepth)).ToList();
            return Ok(new CategoryListRepresentation(list));
        }

        public IHttpActionResult Get(int id)
        {
            var e = _repository.Get(id);
            if (e != null) return Ok(new CategoryRepresentation(e, MaxDepth));
            return NotFound();
        }
        
        public IHttpActionResult Post(CategoryRepresentation category)
        {
            try
            {
                _repository.Add(category.CreateEntity());
                return Ok();
            }
            catch(Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        public IHttpActionResult Put(int id, CategoryRepresentation category)
        {
            if (id != category.ID) return BadRequest();

            try
            {
                _repository.Update(category.CreateEntity());
                return Ok();
            }
            catch (Exception e)
            {
                if (_repository.Get(id) == null) return NotFound();
                return BadRequest(e.Message);
            }
        }

        public IHttpActionResult Delete(int id)
        {
            var category = _repository.Get(id);
            if (category == null) return NotFound();
            try
            {
                _repository.Delete(category);
                return Ok();
            }
            catch (Exception e)
            {
                if (_repository.Get(id) == null) return NotFound();
                return BadRequest(e.Message);
            }
        }
    }
}