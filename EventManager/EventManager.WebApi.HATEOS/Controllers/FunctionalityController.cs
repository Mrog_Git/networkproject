﻿using System.Web.Http;
using EventManager.WebApi.HATEOS.Resources;

namespace EventManager.WebApi.HATEOS.Controllers
{
    public class FunctionalityController : ApiController
    {
        public IHttpActionResult Get()
        {
            return Ok(new FunctionalityRepresentation());
        }
    }
}