﻿using System;
using System.Linq;
using System.Web.Http;
using EventManager.Model.EF.Entities;
using EventManager.Model.EF.Repository;
using EventManager.WebApi.HATEOS.Resources;

namespace EventManager.WebApi.HATEOS.Controllers
{
    public class HumanController : ApiController
    {
        private readonly CrudRepository<Person> _repository = new CrudRepository<Person>();
        private readonly int MaxDepth;

        public HumanController()
        {
            MaxDepth = 4;
        }

        public IHttpActionResult Get()
        {
            var list = _repository.GetAll().Select(c => new PersonRepresentation(c, MaxDepth)).ToList();
            return Ok(new PersonListRepresentation(list));
        }

        public IHttpActionResult Get(int id)
        {
            var e = _repository.Get(id);
            if (e != null) return Ok(new PersonRepresentation(e, MaxDepth));
            return NotFound();
        }

        public IHttpActionResult Post(PersonRepresentation person)
        {
            try
            {
                _repository.Add(person.CreateEntity());
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        public IHttpActionResult Put(int id, PersonRepresentation person)
        {
            if (id != person.ID) return BadRequest();

            try
            {
                _repository.Update(person.CreateEntity());
                return Ok();
            }
            catch (Exception)
            {
                if (_repository.Get(id) == null) return NotFound();
                throw;
            }
        }

        public IHttpActionResult Delete(int id)
        {
            var person = _repository.Get(id);
            if (person == null) return NotFound();
            try
            {
                _repository.Delete(person);
                return Ok();
            }
            catch (Exception e)
            {
                if (_repository.Get(id) == null) return NotFound();
                return BadRequest(e.Message);
            }
        }
    }
}