﻿using System.Web.Http;
using EventManager.Model.DTO;

namespace EventManager.WebApi.ASP
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            DTOConverter.Initialize();
             GlobalConfiguration.Configure(WebApiConfig.Register);
        }
    }
}
