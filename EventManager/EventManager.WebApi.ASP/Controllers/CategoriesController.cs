﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using EventManager.Model.DTO;
using EventManager.Model.EF.Entities;
using EventManager.Model.EF.Repository;

namespace EventManager.WebApi.ASP.Controllers
{
    public class CategoriesController : ApiController
    {
        private readonly CrudRepository<Category> _repository = new CrudRepository<Category>();

        //[CallFrequencyLimit(Name = "CategoryGetAll")]
        public IHttpActionResult Get()
        {
            return Ok((List<CategoryDTO>)DTOConverter.Convert(_repository.GetAll()));
        }

        public IHttpActionResult Get(int id)
        {
            var e = _repository.Get(id);
            if (e != null) return Ok((CategoryDTO)DTOConverter.Convert(e));
            return NotFound();
        }
        
        public IHttpActionResult Post(CategoryDTO category)
        {
            try
            {
                _repository.Add((Category)DTOConverter.Convert(category));
                return Ok();
            }
            catch(Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        public IHttpActionResult Put(int id, CategoryDTO category)
        {
            if (id != category.ID) return BadRequest();

            try
            {
                _repository.Update((Category)DTOConverter.Convert(category));
                return Ok();
            }
            catch (Exception e)
            {
                if (_repository.Get(id) == null) return NotFound();
                return BadRequest(e.Message);
            }
        }

        public IHttpActionResult Delete(int id)
        {
            var category = _repository.Get(id);
            if (category == null) return NotFound();
            try
            {
                _repository.Delete(category);
                return Ok();
            }
            catch (Exception e)
            {
                if (_repository.Get(id) == null) return NotFound();
                return BadRequest(e.Message);
            }
        }
    }
}