﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using EventManager.Model.DTO;
using EventManager.Model.EF.Entities;
using EventManager.Model.EF.Repository;

namespace EventManager.WebApi.ASP.Controllers
{
    public class EventsController : ApiController
    {
        private readonly CrudRepository<Event> _repository = new CrudRepository<Event>();

        public IHttpActionResult Get()
        {
            return Ok((List<EventDTO>)DTOConverter.Convert(_repository.GetAll()));
        }

        public IHttpActionResult Get(int id)
        {
            var e = _repository.Get(id);
            if (e != null) return Ok((EventDTO)DTOConverter.Convert(e));
            return NotFound();
        }

        public IHttpActionResult Post(EventDTO evt)
        {
            try
            {
                _repository.Add((Event)DTOConverter.Convert(evt));
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        public IHttpActionResult Put(int id, EventDTO evt)
        {
            if (id != evt.ID) return BadRequest();

            try
            {
                _repository.Update((Event)DTOConverter.Convert(evt));
                return Ok();
            }
            catch (Exception e)
            {
                if (_repository.Get(id) == null) return NotFound();
                return BadRequest(e.Message);
            }
        }

        public IHttpActionResult Delete(int id)
        {
            var evt = _repository.Get(id);
            if (evt == null) return NotFound();
            try
            {
                _repository.Delete(evt);
                return Ok();
            }
            catch (Exception e)
            {
                if (_repository.Get(id) == null) return NotFound();
                return BadRequest(e.Message);
            }
        }
    }
}