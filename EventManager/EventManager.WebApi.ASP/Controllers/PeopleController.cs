﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Web.Http;
using EventManager.Model.DTO;
using EventManager.Model.EF.Entities;
using EventManager.Model.EF.Repository;

namespace EventManager.WebApi.ASP.Controllers
{
    public class PeopleController : ApiController
    {
        private readonly CrudRepository<Person> _repository = new CrudRepository<Person>();

        public IHttpActionResult Get()
        {
            return Ok((List<PersonDTO>)DTOConverter.Convert(_repository.GetAll()));
        }

        public IHttpActionResult Get(int id)
        {
            var e = _repository.Get(id);
            if (e != null) return Ok((PersonDTO)DTOConverter.Convert(e));
            return NotFound();
        }

        public IHttpActionResult Post(PersonDTO person)
        {
            try
            {
                _repository.Add((Person)DTOConverter.Convert(person));
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        public IHttpActionResult Put(int id, PersonDTO person)
        {
            if (id != person.ID) return BadRequest();

            try
            {
                _repository.Update((Person)DTOConverter.Convert(person));
                return Ok();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (_repository.Get(id) == null) return NotFound();
                throw;
            }
        }

        public IHttpActionResult Delete(int id)
        {
            var person = _repository.Get(id);
            if (person == null) return NotFound();
            try
            {
                _repository.Delete(person);
                return Ok();
            }
            catch (Exception e)
            {
                if (_repository.Get(id) == null) return NotFound();
                return BadRequest(e.Message);
            }
        }
    }
}