﻿using System;
using System.Configuration;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Caching;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace EventManager.WebApi.ASP
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false)]
    public sealed class CallFrequencyLimitAttribute: ActionFilterAttribute
    {
        private readonly int _period;
        private readonly int _count;

        public string Name { get; set; }
        public string Message { get; set; }

        public CallFrequencyLimitAttribute()
        {
            try
            {
                _count = int.Parse(ConfigurationManager.AppSettings.Get("allowedActionCallCountInPeriod"));
                _period = int.Parse(ConfigurationManager.AppSettings.Get("actionCallingPeriod"));
            }
            catch
            {
                _count = 15;
                _period = 10;
            }
        }

        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            var key = string.Concat(Name, "-", ((HttpContextWrapper)actionContext.Request.Properties["MS_HttpContext"]).Request.UserHostAddress);
            var allowExecute = false;

            if (HttpRuntime.Cache[key] == null)
            {
                HttpRuntime.Cache.Add(key,
                    1, 
                    null, 
                    DateTime.Now.AddSeconds(_period), 
                    Cache.NoSlidingExpiration,
                    CacheItemPriority.Low,
                    null);

                allowExecute = true;
            }
            else
                if ((int) HttpRuntime.Cache[key] < _count)
                {
                    HttpRuntime.Cache[key] = (int) HttpRuntime.Cache[key] + 1;
                    allowExecute = true;
                }

            if (allowExecute) return;
            if (string.IsNullOrEmpty(Message))
            {
                Message = "You may perform this action only {n} times in {k} seconds.";
            }

            actionContext.Response = actionContext.Request.CreateResponse(
                HttpStatusCode.Forbidden,
                Message.Replace("{n}", _count.ToString()).Replace("{k}", _period.ToString())
            );
        }
    }
}