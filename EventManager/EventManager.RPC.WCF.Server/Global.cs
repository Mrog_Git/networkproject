﻿using System;
using System.ServiceModel;
using EventManager.Model.DTO;
using EventManager.RPC.WCF.Services.EF;

namespace EventManager.RPC.WCF.Server
{
    class Global
    {
        static void Main(string[] args)
        {
            DTOConverter.Initialize();

            try
            {
                ServiceHost eventHost = new ServiceHost(typeof(EventService));
                eventHost.Open();

                ServiceHost categoryHost = new ServiceHost(typeof(CategoryService));
                categoryHost.Open();

                ServiceHost personHost = new ServiceHost(typeof(PersonService));
                personHost.Open();

                Console.WriteLine("Service up and running at:");
                foreach (var ea in eventHost.Description.Endpoints)
                {
                    Console.WriteLine(ea.Address);
                }
                foreach (var ea in categoryHost.Description.Endpoints)
                {
                    Console.WriteLine(ea.Address);
                }
                foreach (var ea in personHost.Description.Endpoints)
                {
                    Console.WriteLine(ea.Address);
                }

                Console.ReadLine();
                eventHost.Close();
                categoryHost.Close();
                personHost.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.ReadLine();
            }
        }
    }
}
