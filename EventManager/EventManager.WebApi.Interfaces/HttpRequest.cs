﻿using System.Collections.Generic;

namespace EventManager.WebApi.Interfaces
{
    public enum RequestMethod
    {
        GET,
        PUT,
        POST,
        DELETE,
        NULL
    }

    public class HttpRequest
    {
        public string[] Uri { get; set; }
        public IDictionary<string, string> Headers { get; set; }
        public RequestMethod Method { get; set; }
        public string Content { get; set; }
        public object[] Parameters { get; set; }

        public HttpRequest()
        {
            Headers = new Dictionary<string, string>();
        }
    }
}
