﻿namespace EventManager.WebApi.Interfaces
{
    public interface IHttpMessageParser
    {
        HttpRequest ParseRequest(string request);
        string BuildResponse(HttpResponse response, HttpRequest httpRequest = null);
    }
}
