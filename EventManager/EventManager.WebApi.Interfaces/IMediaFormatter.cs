﻿using System;
using System.Collections.Generic;

namespace EventManager.WebApi.Interfaces
{
    public interface IMediaFormatter
    {
        IDictionary<string, ISerializer> SerializerMimes { get; }
        ISerializer DefaultSerializer { get; }

        string GetResponseMime(string acceptMime);
        object DeserializeContent(string mime, string content, Type type);
        string SerializeContent(string mime, object obj);
    }
}
