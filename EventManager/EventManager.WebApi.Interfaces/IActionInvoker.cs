﻿using System.Reflection;
using System.Threading.Tasks;

namespace EventManager.WebApi.Interfaces
{
    public interface IActionInvoker
    {
        IMediaFormatter Formatter { get; }
        Task<HttpResponse> InvokeAction(MethodInfo method, HttpRequest request);
    }
}
