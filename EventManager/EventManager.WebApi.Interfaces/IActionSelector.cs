﻿using System;
using System.Reflection;

namespace EventManager.WebApi.Interfaces
{
    public interface IActionSelector
    {
        MethodInfo GetAction(Type controller, HttpRequest request);
    }
}
