﻿using System;

namespace EventManager.WebApi.Interfaces
{
    public interface ISerializer
    {
        string Serialize(object obj);
        object Deserialize(string s, Type type);
    }
}
