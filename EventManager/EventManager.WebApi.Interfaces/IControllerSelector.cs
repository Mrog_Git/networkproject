﻿using System;
using System.Collections.Generic;

namespace EventManager.WebApi.Interfaces
{
    public interface IControllerSelector
    {
        Type GetController(IEnumerable<Type> controllers, HttpRequest request);
    }
}
