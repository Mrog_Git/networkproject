﻿namespace EventManager.WebApi.Interfaces
{
    public enum StatusCode
    {
        Ok = 200,
        BadRequest = 400,
        NotFound = 404
    }

    public class HttpResponse
    {
        public StatusCode StatusCode { get; set; }
        public object Content { get; set; }

        public HttpResponse()
        {
            
        }

        public HttpResponse(StatusCode code, object content = null)
        {
            StatusCode = code;
            Content = content;
        }
    }
}
