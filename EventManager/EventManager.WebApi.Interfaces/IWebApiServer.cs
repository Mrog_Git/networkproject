﻿using System.Threading.Tasks;

namespace EventManager.WebApi.Interfaces
{
    public interface IWebApiServer
    {
        IControllerSelector ControllerSelector { get; }
        IActionSelector ActionSelector { get; }
        IActionInvoker ActionInvoker { get; }
        IHttpMessageParser MessageParser { get; }
        IMediaFormatter MediaFormatter { get; }

        Task<string> ProcessRequest(string request);
    }
}
