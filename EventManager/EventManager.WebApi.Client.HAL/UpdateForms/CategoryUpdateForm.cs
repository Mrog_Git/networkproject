﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using EventManager.WebApi.Client.HAL.Services;
using EventManager.WebApi.HATEOS.Resources;

namespace EventManager.WebApi.Client.HAL.UpdateForms
{
    public partial class CategoryUpdateForm : Form
    {
        private CategoryRepresentation _activeCategory;
        private readonly IService<CategoryRepresentation> _categoryService;
        private readonly IService<EventRepresentation> _eventService;

        public CategoryUpdateForm(HalClient client)
        {
            _categoryService = new CategoryService(client);
            _eventService = new EventService(client);

            InitializeComponent();

            if (!_eventService.IsRetrieveSupported() || !_categoryService.IsRetrieveSupported() ||
                !_categoryService.IsUpdateSupported())
            {
                MessageBox.Show(@"Some methods are not supported.");
                Close();
            }
            if (!_categoryService.IsDeleteSupported()) btnDelete.Enabled = false;

        }

        private void Reset()
        {
            tbName.Text = "";
            tbName.Enabled = false;

            clbEvents.ClearSelected();

            clbEvents.Enabled = false;
            btnDelete.Enabled = false;
            btnUpdate.Enabled = false;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private async void btnUpdate_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(tbName.Text))
                MessageBox.Show(@"Entered data is not full", @"Enter data!",
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            else
            {
                _activeCategory.Name = tbName.Text;

                _activeCategory.Events.Clear();
                foreach (var i in clbEvents.CheckedIndices)
                {
                    _activeCategory.Events.Add((clbEvents.DataSource as List<EventRepresentation>)?[(int)i]);
                }

                await _categoryService.Update(_activeCategory);

                Close();
            }
        }

        private async void btnChoose_Click(object sender, EventArgs e)
        {
            try
            {
                _activeCategory = await _categoryService.Get(Convert.ToInt32(tbId.Text));
                
                if (_activeCategory != null)
                {
                    clbEvents.Enabled = true;
                    tbName.Enabled = true;

                    tbName.Text = _activeCategory.Name;

                    for (var i = 0; i < clbEvents.Items.Count; i++)
                    {
                        clbEvents.SetItemChecked(i,
                            _activeCategory.Events.Any(m => m.ID == ((EventRepresentation) clbEvents.Items[i]).ID));
                    }

                    btnUpdate.Enabled = true;
                    btnDelete.Enabled = true;
                }
                else MessageBox.Show($@"There is no category with ID = {Convert.ToInt32(tbId.Text)}");

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void tbId_TextChanged(object sender, EventArgs e)
        {
            Reset();
        }

        private async void btnDelete_Click(object sender, EventArgs e)
        {
            await _categoryService.Delete(_activeCategory.ID);

            Close();
        }

        private async void CategoryUpdateForm_Load(object sender, EventArgs e)
        {
            clbEvents.DataSource = await _eventService.Get();

            Reset();
        }
    }
}
