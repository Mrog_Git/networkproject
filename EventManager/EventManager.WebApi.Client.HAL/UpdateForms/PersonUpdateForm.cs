﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using EventManager.WebApi.Client.HAL.Services;
using EventManager.WebApi.HATEOS.Resources;

namespace EventManager.WebApi.Client.HAL.UpdateForms
{
    public partial class PersonUpdateForm : Form
    {
        private PersonRepresentation _activePerson;
        private readonly IService<PersonRepresentation> _personService;
        private readonly IService<EventRepresentation> _eventService;

        public PersonUpdateForm(HalClient client)
        {
            _personService = new PersonService(client);
            _eventService = new EventService(client);

            InitializeComponent();

            if (!_eventService.IsRetrieveSupported() || !_personService.IsRetrieveSupported() ||
                !_personService.IsUpdateSupported())
            {
                MessageBox.Show(@"Some methods are not supported.");
                Close();
            }
            if (!_personService.IsDeleteSupported()) btnDelete.Enabled = false;

        }

        private void Reset()
        {
            tbName.Text = "";
            tbName.Enabled = false;

            clbCreatedEvents.ClearSelected();
            clbVisitedEvents.ClearSelected();

            clbCreatedEvents.Enabled = false;
            clbVisitedEvents.Enabled = false;
            btnDelete.Enabled = false;
            btnUpdate.Enabled = false;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private async void btnUpdate_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(tbName.Text))
                MessageBox.Show(@"Entered data is not full", @"Enter data!",
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            else
            {
                _activePerson.Name = tbName.Text;

                _activePerson.CreatedEvents.Clear();
                foreach (var i in clbCreatedEvents.CheckedIndices)
                {
                    _activePerson.CreatedEvents.Add((clbCreatedEvents.DataSource as List<EventRepresentation>)?[(int)i]);
                }

                _activePerson.VisitedEvents.Clear();
                foreach (var i in clbVisitedEvents.CheckedIndices)
                {
                    _activePerson.VisitedEvents.Add((clbVisitedEvents.DataSource as List<EventRepresentation>)?[(int)i]);
                }

                await _personService.Update(_activePerson);

                Close();
            }
        }

        private async void btnChoose_Click(object sender, EventArgs e)
        {
            try
            {
                _activePerson = await _personService.Get(Convert.ToInt32(tbId.Text));

                if (_activePerson != null)
                {
                    clbCreatedEvents.Enabled = true;
                    clbVisitedEvents.Enabled = true;
                    tbName.Enabled = true;

                    tbName.Text = _activePerson.Name;

                    for (var i = 0; i < clbCreatedEvents.Items.Count; i++)
                    {
                        clbCreatedEvents.SetItemChecked(i,
                            _activePerson.CreatedEvents.Any(m => m.ID == ((EventRepresentation)clbCreatedEvents.Items[i]).ID));
                    }
                    for (var i = 0; i < clbVisitedEvents.Items.Count; i++)
                    {
                        clbVisitedEvents.SetItemChecked(i,
                            _activePerson.VisitedEvents.Any(m => m.ID == ((EventRepresentation)clbVisitedEvents.Items[i]).ID));
                    }

                    btnUpdate.Enabled = true;
                    btnDelete.Enabled = true;
                }
                else MessageBox.Show($@"There is no category with ID = {Convert.ToInt32(tbId.Text)}");

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void tbId_TextChanged(object sender, EventArgs e)
        {
            Reset();
        }

        private async void btnDelete_Click(object sender, EventArgs e)
        {
            await _personService.Delete(_activePerson.ID);

            Close();
        }

        private async void PersonUpdateForm_Load(object sender, EventArgs e)
        {
            clbCreatedEvents.DataSource = await _eventService.Get();
            clbVisitedEvents.DataSource = await _eventService.Get();

            Reset();
        }
    }
}
