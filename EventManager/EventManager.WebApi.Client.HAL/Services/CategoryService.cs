﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using EventManager.WebApi.HATEOS.Resources;
using Tavis.UriTemplates;

namespace EventManager.WebApi.Client.HAL.Services
{
    public class CategoryService: IService<CategoryRepresentation>
    {
        private readonly HalClient _client;

        public CategoryService(HalClient client)
        {
            _client = client;
        }

        public bool IsCreateSupported()
        {
            return _client.IsCommandSupported("categories:create");
        }

        public bool IsDeleteSupported()
        {
            return _client.IsCommandSupported("categories:delete");
        }

        public bool IsRetrieveSupported()
        {
            return _client.IsCommandSupported("categories:retrieve");
        }

        public bool IsUpdateSupported()
        {
            return _client.IsCommandSupported("categories:update");
        }

        public bool IsGetListSupported()
        {
            return _client.IsCommandSupported("categories:list");
        }

        public async Task<List<CategoryRepresentation>> Get()
        {
            var link = _client.GetLink("categories:list");
            var uri = _client.GetUri(link);
            var response = await _client.Get(uri);
            return response.Resource.CreateCategories();
        }

        public async Task<CategoryRepresentation> Get(int id)
        {
            var link = _client.GetLink("categories:retrieve");
            link = link.ResolveTemplated(x =>
            {
                x.AddParameter("id", id);
                return x.Resolve();
            });
            var uri = _client.GetUri(link);
            var response = await _client.Get(uri);
            return response.Resource.CreateCategory();
        }

        public async Task Delete(int id)
        {
            var link = _client.GetLink("categories:delete");
            link = link.ResolveTemplated(x =>
            {
                x.AddParameter("id", id);
                return x.Resolve();
            });
            var uri = _client.GetUri(link);
            await _client.Delete(uri);
        }

        public async Task Update(CategoryRepresentation resource)
        {
            var link = _client.GetLink("categories:update");
            link = link.ResolveTemplated(x =>
            {
                x.AddParameter("id", resource.ID);
                return x.Resolve();
            });
            var uri = _client.GetUri(link);
            await _client.Put(uri, resource);
        }

        public async Task Add(CategoryRepresentation resource)
        {
            var link = _client.GetLink("categories:create");
            var uri = _client.GetUri(link);
            await _client.Post(uri, resource);
        }
    }
}
