﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace EventManager.WebApi.Client.HAL.Services
{
    public interface IService<T>
    {
        bool IsCreateSupported();
        bool IsDeleteSupported();
        bool IsRetrieveSupported();
        bool IsUpdateSupported();
        bool IsGetListSupported();

        Task<List<T>> Get();
        Task<T> Get(int id);
        Task Delete(int id);
        Task Update(T resource);
        Task Add(T resource);
    }
}
