﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using EventManager.WebApi.HATEOS.Resources;
using Tavis.UriTemplates;

namespace EventManager.WebApi.Client.HAL.Services
{
    public class PersonService: IService<PersonRepresentation>
    {
        private readonly HalClient _client;

        public PersonService(HalClient client)
        {
            _client = client;
        }

        public bool IsCreateSupported()
        {
            return _client.IsCommandSupported("people:create");
        }

        public bool IsDeleteSupported()
        {
            return _client.IsCommandSupported("people:delete");
        }

        public bool IsRetrieveSupported()
        {
            return _client.IsCommandSupported("people:retrieve");
        }

        public bool IsUpdateSupported()
        {
            return _client.IsCommandSupported("people:update");
        }

        public bool IsGetListSupported()
        {
            return _client.IsCommandSupported("people:list");
        }

        public async Task<List<PersonRepresentation>> Get()
        {
            var link = _client.GetLink("people:list");
            var uri = _client.GetUri(link);
            var response = await _client.Get(uri);
            return response.Resource.CreatePeople();
        }

        public async Task<PersonRepresentation> Get(int id)
        {
            var link = _client.GetLink("people:retrieve");
            link = link.ResolveTemplated(x =>
            {
                x.AddParameter("id", id);
                return x.Resolve();
            });
            var uri = _client.GetUri(link);
            var response = await _client.Get(uri);
            return response.Resource.CreatePerson();
        }

        public async Task Delete(int id)
        {
            var link = _client.GetLink("people:delete");
            link = link.ResolveTemplated(x =>
            {
                x.AddParameter("id", id);
                return x.Resolve();
            });
            var uri = _client.GetUri(link);
            await _client.Delete(uri);
        }

        public async Task Update(PersonRepresentation resource)
        {
            var link = _client.GetLink("people:update");
            link = link.ResolveTemplated(x =>
            {
                x.AddParameter("id", resource.ID);
                return x.Resolve();
            });
            var uri = _client.GetUri(link);
            await _client.Put(uri, resource);
        }

        public async Task Add(PersonRepresentation resource)
        {
            var link = _client.GetLink("people:create");
            var uri = _client.GetUri(link);
            await _client.Post(uri, resource);
        }
    }
}
