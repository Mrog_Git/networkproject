﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using EventManager.WebApi.HATEOS.Resources;
using Tavis.UriTemplates;

namespace EventManager.WebApi.Client.HAL.Services
{
    public class EventService: IService<EventRepresentation>
    {
        private HalClient _client;

        public EventService(HalClient client)
        {
            _client = client;
        }

        public bool IsCreateSupported()
        {
            return _client.IsCommandSupported("events:create");
        }

        public bool IsDeleteSupported()
        {
            return _client.IsCommandSupported("events:delete");
        }

        public bool IsRetrieveSupported()
        {
            return _client.IsCommandSupported("events:retrieve");
        }

        public bool IsUpdateSupported()
        {
            return _client.IsCommandSupported("events:update");
        }

        public bool IsGetListSupported()
        {
            return _client.IsCommandSupported("events:list");
        }

        public async Task<List<EventRepresentation>> Get()
        {
            var link = _client.GetLink("events:list");
            var uri = _client.GetUri(link);
            var response = await _client.Get(uri);
            return response.Resource.CreateEvents();
        }

        public async Task<EventRepresentation> Get(int id)
        {
            var link = _client.GetLink("events:retrieve");
            link = link.ResolveTemplated(x =>
            {
                x.AddParameter("id", id);
                return x.Resolve();
            });
            var uri = _client.GetUri(link);
            var response = await _client.Get(uri);
            return response.Resource.CreateEvent();
        }

        public async Task Delete(int id)
        {
            var link = _client.GetLink("events:delete");
            link = link.ResolveTemplated(x =>
            {
                x.AddParameter("id", id);
                return x.Resolve();
            });
            var uri = _client.GetUri(link);
            await _client.Delete(uri);
        }

        public async Task Update(EventRepresentation resource)
        {
            var link = _client.GetLink("events:update");
            link = link.ResolveTemplated(x =>
            {
                x.AddParameter("id", resource.ID);
                return x.Resolve();
            });
            var uri = _client.GetUri(link);
            await _client.Put(uri, resource);
        }

        public async Task Add(EventRepresentation resource)
        {
            var link = _client.GetLink("events:create");
            var uri = _client.GetUri(link);
            await _client.Post(uri, resource);
        }
    }
}
