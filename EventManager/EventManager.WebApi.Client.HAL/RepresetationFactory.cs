﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EventManager.WebApi.HATEOS.Resources;
using HalClient.Net;
using HalClient.Net.Parser;

namespace EventManager.WebApi.Client.HAL
{
    public static class RepresetationFactory
    {
        public static EventRepresentation CreateEvent(this IResourceObject obj)
        {
            var result = new EventRepresentation()
            {
                ID = Convert.ToInt32(obj.State.SingleOrDefault(kv => kv.Key == "ID").Value.Value),
                Title = obj.State.SingleOrDefault(kv => kv.Key == "Title").Value.Value
            };
            try
            {
                result.Creator_ID = Convert.ToInt32(obj.State.SingleOrDefault(kv => kv.Key == "Creator_ID").Value.Value);
            }
            catch
            {
                result.Creator_ID = null;
            }
            try
            {
                result.Category_ID = Convert.ToInt32(obj.State.SingleOrDefault(kv => kv.Key == "Category_ID").Value.Value);
            }
            catch
            {
                result.Creator_ID = null;
            }
            if (result.Category_ID != null) result.Category = obj.CreateCategories().FirstOrDefault();
            if (result.Creator_ID != null) result.Creator = obj.CreatePeople().FirstOrDefault(p => p.ID == result.Creator_ID);
            result.Members = obj.CreateMembers();

            return result;
        }

        public static List<EventRepresentation> CreateEvents(this IResourceObject obj)
        {
            var list = new List<EventRepresentation>();

            if (obj.Embedded.Any(kv => kv.Key == "event"))
                foreach (var cat in obj.Embedded.SingleOrDefault(kv => kv.Key == "event").Value)
                {
                    var resource = cat.CreateEvent();
                    list.Add(resource);
                }
            return list;
        }

        public static List<EventRepresentation> CreateCreatedEvents(this IResourceObject obj)
        {
            var list = new List<EventRepresentation>();

            if (obj.Embedded.Any(kv => kv.Key == "created_event"))
                foreach (var cat in obj.Embedded.SingleOrDefault(kv => kv.Key == "created_event").Value)
                {
                    var resource = cat.CreateEvent();
                    list.Add(resource);
                }
            return list;
        }

        public static List<EventRepresentation> CreateVisitedEvents(this IResourceObject obj)
        {
            var list = new List<EventRepresentation>();

            if (obj.Embedded.Any(kv => kv.Key == "visited_event"))
                foreach (var cat in obj.Embedded.SingleOrDefault(kv => kv.Key == "visited_event").Value)
                {
                    var resource = cat.CreateEvent();
                    list.Add(resource);
                }
            return list;
        }

        public static CategoryRepresentation CreateCategory(this IResourceObject obj)
        {
            var result = new CategoryRepresentation()
            {
                ID = Convert.ToInt32(obj.State.SingleOrDefault(kv => kv.Key == "ID").Value.Value),
                Name = obj.State.SingleOrDefault(kv => kv.Key == "Name").Value.Value
            };
            result.Events.AddRange(obj.CreateEvents());

            return result;
        }

        public static List<CategoryRepresentation> CreateCategories(this IResourceObject obj)
        {
            var list = new List<CategoryRepresentation>();

            if (obj.Embedded.Any(kv => kv.Key == "category"))
                foreach (var cat in obj.Embedded.SingleOrDefault(kv => kv.Key == "category").Value)
                {
                    var resource = cat.CreateCategory();
                    list.Add(resource);
                }
            return list;
        }

        public static PersonRepresentation CreatePerson(this IResourceObject obj)
        {
            var result = new PersonRepresentation()
            {
                ID = Convert.ToInt32(obj.State.SingleOrDefault(kv => kv.Key == "ID").Value.Value),
                Name = obj.State.SingleOrDefault(kv => kv.Key == "Name").Value.Value
            };
            result.CreatedEvents.AddRange(obj.CreateCreatedEvents());
            result.VisitedEvents.AddRange(obj.CreateVisitedEvents());

            return result;
        }

        public static List<PersonRepresentation> CreatePeople(this IResourceObject obj)
        {
            var list = new List<PersonRepresentation>();

            if (obj.Embedded.Any(kv => kv.Key == "person"))
                foreach (var cat in obj.Embedded.SingleOrDefault(kv => kv.Key == "person").Value)
                {
                    var resource = cat.CreatePerson();
                    list.Add(resource);
                }
            return list;
        }

        public static List<PersonRepresentation> CreateMembers(this IResourceObject obj)
        {
            var list = new List<PersonRepresentation>();

            if (obj.Embedded.Any(kv => kv.Key == "member"))
                foreach (var cat in obj.Embedded.SingleOrDefault(kv => kv.Key == "member").Value)
                {
                    var resource = cat.CreatePerson();
                    list.Add(resource);
                }
            return list;
        }
    }
}
