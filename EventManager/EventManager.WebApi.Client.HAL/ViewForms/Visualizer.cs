﻿using System.Collections.Generic;
using System.Windows.Forms;
using EventManager.WebApi.HATEOS.Resources;

namespace EventManager.WebApi.Client.HAL.ViewForms
{
    public static class Visualizer
    {
        public static TreeNode[] Visualize(this IEnumerable<EventRepresentation> source, int depth, int curLevel = 0)
        {
            var result = new List<TreeNode>();

            foreach (var eventDto in source)
            {
                if (curLevel <= depth) result.Add(eventDto.Visualize(depth,curLevel));
            }

            return result.ToArray();
        }

        public static TreeNode[] Visualize(this IEnumerable<PersonRepresentation> source, int depth, int curLevel = 0)
        {
            var result = new List<TreeNode>();

            foreach (var personDto in source)
            {
                if (curLevel <= depth) result.Add(personDto.Visualize(depth, curLevel));
            }

            return result.ToArray();
        }

        public static TreeNode[] Visualize(this IEnumerable<CategoryRepresentation> source, int depth, int curLevel = 0)
        {
            var result = new List<TreeNode>();

            foreach (var categoryDto in source)
            {
                if (curLevel <= depth) result.Add(categoryDto.Visualize(depth, curLevel));
            }

            return result.ToArray();
        }

        public static TreeNode Visualize(this EventRepresentation source, int depth, int curLevel = 0)
        {
            curLevel++;
            var result = new TreeNode(source.ToString());

            if (curLevel <= depth)
            {
                if (source.Category_ID == null) result.Nodes.Add("Category: Others (Without category)");
                else
                {
                    var n = source.Category.Visualize(depth, curLevel);
                    n.Text = @"Category: " + n.Text;
                    result.Nodes.Add(n);
                }

                if (source.Creator_ID == null) result.Nodes.Add("Without creator");
                else
                {
                    var n = source.Creator.Visualize(depth, curLevel);
                    n.Text = @"Creator: " + n.Text;
                    result.Nodes.Add(n);
                }

                var node = new TreeNode($"Members ({source.Members.Count} items)");
                node.Nodes.AddRange(source.Members.Visualize(depth,curLevel));
                result.Nodes.Add(node);
            }

            return result;
        }

        public static TreeNode Visualize(this PersonRepresentation source, int depth, int curLevel = 0)
        {
            curLevel++;
            var result = new TreeNode(source.ToString());

            if (curLevel <= depth)
            {
                var createdEventsNode = new TreeNode($"Created Events ({source.CreatedEvents.Count} items)");
                createdEventsNode.Nodes.AddRange(source.CreatedEvents.Visualize(depth,curLevel));
                result.Nodes.Add(createdEventsNode);

                var visitedEventsNode = new TreeNode($"Visited Events ({source.VisitedEvents.Count} items)");
                visitedEventsNode.Nodes.AddRange(source.VisitedEvents.Visualize(depth, curLevel));
                result.Nodes.Add(visitedEventsNode);
            }

            return result;
        }

        public static TreeNode Visualize(this CategoryRepresentation source, int depth, int curLevel = 0)
        {
            curLevel++;
            var result = new TreeNode(source.ToString());

            if (curLevel <= depth)
            {
                var node = new TreeNode($"Events ({source.Events.Count} items)");
                node.Nodes.AddRange(source.Events.Visualize(depth,curLevel));
                result.Nodes.Add(node);
            }

            return result;
        }
    }
}
