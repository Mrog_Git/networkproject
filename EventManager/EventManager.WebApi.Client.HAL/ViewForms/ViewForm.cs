﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Forms;
using EventManager.WebApi.Client.HAL.Services;
using EventManager.WebApi.HATEOS.Resources;

namespace EventManager.WebApi.Client.HAL.ViewForms
{
    public partial class ViewForm : Form
    {
        public enum EntityType
        {
            Event,
            Person,
            Category
        }

        private readonly EntityType _activeEntity;
        private readonly IService<EventRepresentation> _eventService;
        private readonly IService<PersonRepresentation> _personService;
        private readonly IService<CategoryRepresentation> _categoryService;

        public ViewForm(HalClient client, EntityType entityType)
        {
            _eventService = new EventService(client);
            _personService = new PersonService(client);
            _categoryService = new CategoryService(client);
            if ((entityType == EntityType.Category && !_categoryService.IsRetrieveSupported()) ||
                (entityType == EntityType.Person && !_personService.IsRetrieveSupported()) ||
                (entityType == EntityType.Event && !_eventService.IsRetrieveSupported()))
            {
                MessageBox.Show(@"Some methods are not supported.");
                Close();
            }
            InitializeComponent();
            _activeEntity = entityType;
        }

        private async void btnAll_Click(object sender, EventArgs e)
        {
            try
            {
                int depth = Convert.ToInt32(tbDepth.Text);

                tvEvents.Nodes.Clear();
                if (_activeEntity == EntityType.Event) await AddEventsToTree(depth);
                else if (_activeEntity == EntityType.Category) await AddCategoriesToTree(depth);
                else await AddPersonsToTree(depth);
                tvEvents.Invalidate();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
            
        }

        private async Task AddEventsToTree(int depth)
        {
            List<EventRepresentation> source = await _eventService.Get();
            tvEvents.Nodes.AddRange(source.Visualize(depth));
        }

        private async Task AddPersonsToTree(int depth)
        {
            List<PersonRepresentation> source = await _personService.Get();
            tvEvents.Nodes.AddRange(source.Visualize(depth));
        }

        private async Task AddCategoriesToTree(int depth)
        {
            List<CategoryRepresentation> source = await _categoryService.Get();
            tvEvents.Nodes.AddRange(source.Visualize(depth));
        }

        private async void btnId_Click(object sender, EventArgs e)
        {
            try
            {
                int depth = Convert.ToInt32(tbDepth.Text);
                int id = Convert.ToInt32(tbId.Text);

                tvEvents.Nodes.Clear();
                if (_activeEntity == EntityType.Event) await AddEventToTree(depth,id);
                else if (_activeEntity == EntityType.Category) await AddCategoryToTree(depth,id);
                else await AddPersonToTree(depth,id);
                tvEvents.Invalidate();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        private async Task AddEventToTree(int depth, int id)
        {
            var evt = await _eventService.Get(id);
            if (evt == null) MessageBox.Show($@"There is no event with ID = {id}");
            else tvEvents.Nodes.Add(evt.Visualize(depth));

        }

        private async Task AddPersonToTree(int depth, int id)
        {
            var evt = await _personService.Get(id);
            if (evt == null) MessageBox.Show($@"There is no event with ID = {id}");
            else tvEvents.Nodes.Add(evt.Visualize(depth));

        }

        private async Task AddCategoryToTree(int depth, int id)
        {
            var evt = await _categoryService.Get(id);
            if (evt == null) MessageBox.Show($@"There is no event with ID = {id}");
            else tvEvents.Nodes.Add(evt.Visualize(depth));

        }
    }
}
