﻿using System;
using System.Net;
using System.Windows.Forms;
using EventManager.WebApi.Client.HAL.CreateForms;
using EventManager.WebApi.Client.HAL.Services;
using EventManager.WebApi.Client.HAL.UpdateForms;
using EventManager.WebApi.Client.HAL.ViewForms;

namespace EventManager.WebApi.Client.HAL
{
    public partial class MainForm : Form
    {
        private HalClient _client;
        
        public MainForm()
        {
            InitializeComponent();
        }

        private void btnCreateEvent_Click(object sender, EventArgs e)
        {
            CreateEventFrom form = new CreateEventFrom(_client);
            form.ShowDialog();
        }

        private void btnCreatePerson_Click(object sender, EventArgs e)
        {
            CreatePersonForm form = new CreatePersonForm(_client);
            form.ShowDialog();
        }

        private void btnCreateCategory_Click(object sender, EventArgs e)
        {
            CreateCategoryForm form = new CreateCategoryForm(_client) {Text = @"Category"};
            form.ShowDialog();
        }

        private void btnViewEvents_Click(object sender, EventArgs e)
        {
            ViewForm form = new ViewForm(_client, ViewForm.EntityType.Event) {Text = @"Events"};
            form.ShowDialog();
        }

        private void btnViewPerson_Click(object sender, EventArgs e)
        {
            ViewForm form = new ViewForm(_client, ViewForm.EntityType.Person) {Text = @"Person"};
            form.ShowDialog();
        }

        private void btnUpdateEvent_Click(object sender, EventArgs e)
        {
            EventUpdateForm form = new EventUpdateForm(_client) { Text = @"Person" };
            form.ShowDialog();
        }

        private void btnUpdateCategory_Click(object sender, EventArgs e)
        {
            CategoryUpdateForm form = new CategoryUpdateForm(_client);
            form.ShowDialog();
        }

        private void btnViewCategories_Click(object sender, EventArgs e)
        {
            ViewForm form = new ViewForm(_client, ViewForm.EntityType.Category) { Text = @"Category" };
            form.ShowDialog();
        }

        private void btnUpdatePerson_Click(object sender, EventArgs e)
        {
            PersonUpdateForm form = new PersonUpdateForm(_client);
            form.ShowDialog();
        }

        private async void btnConnect_Click(object sender, EventArgs e)
        {
            _client = new HalClient("http://"+textBox1.Text+":50943");
            try
            {
                await _client.GetFunctionality("/functionality");
                btnConnect.Enabled = false;
                btnCreateCategory.Enabled = true;
                btnCreateEvent.Enabled = true;
                btnCreatePerson.Enabled = true;
                btnUpdateCategory.Enabled = true;
                btnUpdateEvent.Enabled = true;
                btnUpdatePerson.Enabled = true;
                btnViewCategories.Enabled = true;
                btnViewEvents.Enabled = true;
                btnViewPersons.Enabled = true;
            }
            catch (Exception ex)
            {
                _client.Dispose();
                MessageBox.Show(ex.Message);
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            _client?.Dispose();
            btnConnect.Enabled = true;
            btnCreateCategory.Enabled = false;
            btnCreateEvent.Enabled = false;
            btnCreatePerson.Enabled = false;
            btnUpdateCategory.Enabled = false;
            btnUpdateEvent.Enabled = false;
            btnUpdatePerson.Enabled = false;
            btnViewCategories.Enabled = false;
            btnViewEvents.Enabled = false;
            btnViewPersons.Enabled = false;
        }
    }
}
