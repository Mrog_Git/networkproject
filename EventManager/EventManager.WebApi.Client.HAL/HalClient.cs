﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HalClient.Net;
using HalClient.Net.Parser;

namespace EventManager.WebApi.Client.HAL
{
    public class HalClient
    {
        private string Address { get; set; }
        private IHalHttpClient Client { get; set; }
        private IReadOnlyDictionary<string, IEnumerable<ILinkObject>> Functionality { get; set; }

        public HalClient(string address)
        {
            Address = address.Trim('/');

            IHalJsonParser parser = new HalJsonParser();
            IHalHttpClientFactory factory = new HalHttpClientFactory(parser);
            Client = factory.CreateClient();
        }

        public async Task GetFunctionality(string navigationUrl)
        {
            var response = await Client.GetAsync(new Uri(Address + navigationUrl));

            if (response.IsHalResponse)
            {
                Functionality = response.Resource.Links;
            }
            else
            {
                throw new Exception("Invalid navigation url.");
            }
        }

        public async Task<IHalHttpResponseMessage> Get(Uri uri)
        {
            var response = await Client.GetAsync(uri);

            return response;
        }

        public async Task<IHalHttpResponseMessage> Delete(Uri uri)
        {
            var response = await Client.DeleteAsync(uri);

            return response;
        }

        public async Task<IHalHttpResponseMessage> Post(Uri uri, object data)
        {
            var response = await Client.PostAsync(uri, data);

            return response;
        }

        public async Task<IHalHttpResponseMessage> Put(Uri uri, object data)
        {
            var response = await Client.PutAsync(uri, data);

            return response;
        }

        public bool IsCommandSupported(string resource)
        {
            return Functionality.Any(x => x.Key == resource);
        }

        public ILinkObject GetLink(string resource)
        {
            return Functionality.Single(x => x.Key == resource).Value.First();
        }

        public Uri GetUri(ILinkObject linkObject)
        {
            return new Uri(Address + linkObject.Href);
        }

        public void Dispose()
        {
            Client.Dispose();
        }
    }
}
