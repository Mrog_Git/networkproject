﻿using System;
using System.Windows.Forms;
using EventManager.WebApi.Client.HAL.Services;
using EventManager.WebApi.HATEOS.Resources;

namespace EventManager.WebApi.Client.HAL.CreateForms
{
    public partial class CreateCategoryForm : Form
    {
        private readonly IService<CategoryRepresentation> _service;
        
        public CreateCategoryForm(HalClient client)
        {
            _service = new CategoryService(client);
            InitializeComponent();
            if (!_service.IsCreateSupported())
            {
                MessageBox.Show(@"Some methods are not supported.");
                Close();
            }
        }

        private async void btnCreate_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(tbName.Text))
                MessageBox.Show(@"Entered data is not full", @"Enter data!",
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            else
            {
                var newCategory = new CategoryRepresentation() { Name = tbName.Text };

                await _service.Add(newCategory);

                Close();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void CreateCategoryForm_Load(object sender, EventArgs e)
        {

        }
    }
}
