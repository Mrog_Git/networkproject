﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using EventManager.WebApi.Client.HAL.Services;
using EventManager.WebApi.HATEOS.Resources;

namespace EventManager.WebApi.Client.HAL.CreateForms
{
    public partial class CreateEventFrom : Form
    {
        private readonly IService<EventRepresentation> _eventService;
        private readonly IService<CategoryRepresentation> _categoryService;
        private readonly IService<PersonRepresentation> _personService;

        public CreateEventFrom(HalClient client)
        {
            _eventService = new EventService(client);
            _categoryService = new CategoryService(client);
            _personService = new PersonService(client);

            InitializeComponent();
            if (!_eventService.IsCreateSupported() || !_categoryService.IsRetrieveSupported() ||
                !_personService.IsRetrieveSupported())
            {
                MessageBox.Show(@"Some methods are not supported.");
                Close();
            }

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private async void btnCreate_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(tbTitle.Text) ||
                cbCategory.SelectedIndex == -1 ||
                cbCreator.SelectedIndex == -1)
                MessageBox.Show(@"Entered data is not full", @"Enter data!",
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            else
            {
                var newEvent = new EventRepresentation()
                {
                    Title = tbTitle.Text,
                    Category_ID = chNoCategory.Checked
                        ? null
                        : (cbCategory.DataSource as List<CategoryRepresentation>)?[cbCategory.SelectedIndex]?.ID,
                    Creator_ID = chNoCreator.Checked
                        ? null
                        : (cbCreator.DataSource as List<PersonRepresentation>)?[cbCreator.SelectedIndex]?.ID
                };
                foreach (var i in clbMembers.CheckedIndices)
                {
                    newEvent.Members.Add((clbMembers.DataSource as List<PersonRepresentation>)?[(int)i]);
                }

                await _eventService.Add(newEvent);
                
                Close();
            }
        }

        private void chNoCreator_CheckedChanged(object sender, EventArgs e)
        {
            cbCreator.Enabled = !((CheckBox) sender).Checked;
        }

        private void chNoCategory_CheckedChanged(object sender, EventArgs e)
        {
            cbCategory.Enabled = !((CheckBox)sender).Checked;
        }

        private async void CreateEventFrom_Load(object sender, EventArgs e)
        {

            cbCategory.DataSource = await _categoryService.Get();
            cbCreator.DataSource = await _personService.Get();
            clbMembers.DataSource = await _personService.Get();
        }
    }
}
