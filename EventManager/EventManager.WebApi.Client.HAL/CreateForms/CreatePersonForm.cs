﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using EventManager.WebApi.Client.HAL.Services;
using EventManager.WebApi.HATEOS.Resources;

namespace EventManager.WebApi.Client.HAL.CreateForms
{
    public partial class CreatePersonForm : Form
    {
        private readonly IService<EventRepresentation> _eventService;
        private readonly IService<PersonRepresentation> _personService;

        public CreatePersonForm(HalClient client)
        {
            _eventService = new EventService(client);
            _personService = new PersonService(client);

            InitializeComponent();
            if (!_eventService.IsCreateSupported() ||
                !_personService.IsRetrieveSupported())
            {
                MessageBox.Show(@"Some methods are not supported.");
                Close();
            }

        }

        private void btnCancel_Click(object sender, System.EventArgs e)
        {
            Close();
        }

        private async void btnCreate_Click(object sender, System.EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(tbName.Text))
                MessageBox.Show(@"Entered data is not full", @"Enter data!",
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            else
            {
                var newPerson = new PersonRepresentation() {Name = tbName.Text};
                foreach (var i in clbEvents.CheckedIndices)
                {
                    newPerson.VisitedEvents.Add((clbEvents.DataSource as List<EventRepresentation>)?[(int)i]);
                }

                await _personService.Add(newPerson);

                Close();
            }
        }

        private async void CreatePersonForm_Load(object sender, EventArgs e)
        {
            clbEvents.DataSource = await _eventService.Get();

        }
    }
}
