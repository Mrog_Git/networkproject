﻿using System;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using EventManager.WebApi.Interfaces;

namespace EventManager.WebApi.Core
{
    public class ActionInvoker: IActionInvoker
    {
        public IMediaFormatter Formatter { get; }

        public ActionInvoker(IMediaFormatter formatter)
        {
            Formatter = formatter;
        }

        public Task<HttpResponse> InvokeAction(MethodInfo method, HttpRequest request)
        {
            string mime = request.Headers.ContainsKey("content-type") ? request.Headers["content-type"] : null;
            if (method.GetParameters().Any())
            {
                Type contentType = method.GetParameters().Last().ParameterType;

                if (request.Content.Length > 0)
                    request.Parameters[request.Parameters.Length - 1] = Formatter.DeserializeContent(mime, request.Content,
                        contentType);
            }
            return Task.Run(() => (HttpResponse)method.Invoke(Activator.CreateInstance(method.DeclaringType), request.Parameters));
        }

    }
}
