﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using EventManager.WebApi.Interfaces;

namespace EventManager.WebApi.Core
{
    public class HttpMessageParser: IHttpMessageParser
    {
        private IMediaFormatter MediaFormatter;

        public HttpMessageParser(IMediaFormatter mediaFormatter)
        {
            MediaFormatter = mediaFormatter;
        }


        private void ParseHeaders(StringReader reader, HttpRequest request)
        {
            string line;
            do
            {
                line = reader.ReadLine();
                var header = line?.Split(new[] {':'}, 2);
                if (header?.Length> 1) request.Headers.Add(header[0].Trim().ToLower(),header[1].Trim().ToLower());
            } while (!String.IsNullOrEmpty(line));
        }

        private void ParseStartingLine(StringReader reader, HttpRequest request)
        {
            var line = reader.ReadLine();

            var parts = line?.Split(' ');
            if (parts == null || parts.Length < 2) throw new Exception("Invalid starting line in request.");
            request.Method = ParseMethod(parts[0]);
            request.Uri = parts[1].Trim('/').Split('/');
            request.Uri[request.Uri.Length - 1] = request.Uri[request.Uri.Length - 1].Split('?')[0];
        }

        private static RequestMethod ParseMethod(string method)
        {
            method = method.ToLower().Trim();
            var m = method == "get"
                ? RequestMethod.GET
                : (method == "put"
                    ? RequestMethod.PUT
                    : (method == "post"
                        ? RequestMethod.POST
                        : (method == "delete" ? RequestMethod.DELETE : RequestMethod.NULL)));
            if (m == RequestMethod.NULL) throw new Exception("Invalid request method.");
            return m;
        }

        private static string BuildStatusCode(StatusCode code)
        {
            return code == StatusCode.BadRequest
                ? "400 Bad Request"
                : (code == StatusCode.NotFound
                    ? "404 Not Found"
                    : "200 OK");
        }

        public HttpRequest ParseRequest(string request)
        {
            var httpRequest = new HttpRequest();

            using (var reader = new StringReader(request))
            {
                ParseStartingLine(reader,httpRequest);
                ParseHeaders(reader,httpRequest);
                httpRequest.Content = reader.ReadToEnd();
                var parameters = new List<object>();
                for (var i = 1; i < httpRequest.Uri.Length; i++)
                {
                    try
                    {
                        var par = Convert.ToInt32(httpRequest.Uri[i]);
                        parameters.Add(par);
                    }
                    catch (Exception)
                    {
                        parameters.Add(httpRequest.Uri[i]);
                    }
                }

                if (httpRequest.Content.Length > 0) parameters.Add(httpRequest.Content);
                httpRequest.Parameters = parameters.ToArray();

            }

            return httpRequest;
        }

        public string BuildResponse(HttpResponse httpResponse, HttpRequest httpRequest = null)
        {
            StringBuilder response = new StringBuilder();
            response.Append("HTTP/1.1 ");
            response.AppendLine(BuildStatusCode(httpResponse.StatusCode));
            if (httpResponse.Content != null)
            {
                var acceptMime = httpRequest == null
                    ? null
                    : (httpRequest.Headers.ContainsKey("accept") ? httpRequest.Headers["accept"] : null);
                httpResponse.Content = MediaFormatter.SerializeContent(acceptMime, httpResponse.Content);
                response.AppendLine("Content-length: " + Encoding.UTF8.GetBytes((string) httpResponse.Content).Length);
                response.AppendLine("Content-type: " + MediaFormatter.GetResponseMime(acceptMime));
            }
            else httpResponse.Content = "";
            response.AppendLine();
            response.Append((string) httpResponse.Content);
            return response.ToString();
        }
    }
}
