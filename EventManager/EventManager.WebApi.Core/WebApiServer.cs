﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using EventManager.WebApi.Interfaces;

namespace EventManager.WebApi.Core
{
    public class WebApiServer: IWebApiServer
    {
        private readonly IEnumerable<Type> _controllers;

        public IControllerSelector ControllerSelector { get; }
        public IActionSelector ActionSelector { get; }
        public IActionInvoker ActionInvoker { get; }
        public IHttpMessageParser MessageParser { get; }
        public IMediaFormatter MediaFormatter { get; }

        public WebApiServer(IEnumerable<Type> controllers)
        {
            _controllers = controllers;
            ControllerSelector = new ControllerSelector();
            ActionSelector = new ActionSelector(new Dictionary<RequestMethod, string>()
            {
                { RequestMethod.DELETE, "Delete"},
                { RequestMethod.GET, "Get"},
                { RequestMethod.POST, "Post"},
                { RequestMethod.PUT, "Put"}
            });
            MediaFormatter = new MediaFormatter();
            MessageParser = new HttpMessageParser(MediaFormatter);
            ActionInvoker = new ActionInvoker(MediaFormatter);
        }

        public async Task<string> ProcessRequest(string request)
        {
            try
            {
                HttpRequest httpRequest = MessageParser.ParseRequest(request);

                var controller = ControllerSelector.GetController(_controllers, httpRequest);
                if (controller == null) return MessageParser.BuildResponse(new HttpResponse(StatusCode.NotFound, "Required controller is not found."));

                var action = ActionSelector.GetAction(controller, httpRequest);
                if (action == null) return MessageParser.BuildResponse(new HttpResponse(StatusCode.NotFound, "Required action is not found."));

                HttpResponse httpResponse = await ActionInvoker.InvokeAction(action, httpRequest);

                return MessageParser.BuildResponse(httpResponse, httpRequest);
            }
            catch (Exception e)
            {
                return MessageParser.BuildResponse(new HttpResponse(StatusCode.BadRequest, e.Message));
            }
        }

    }
}
