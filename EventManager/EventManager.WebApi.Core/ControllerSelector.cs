﻿using System;
using System.Collections.Generic;
using System.Linq;
using EventManager.WebApi.Interfaces;

namespace EventManager.WebApi.Core
{
    public class ControllerSelector: IControllerSelector
    {
        public Type GetController(IEnumerable<Type> controllers, HttpRequest request)
        {
            if (controllers.Any(c => c.Name.ToLower().StartsWith(request.Uri[0].ToLower())))
                return controllers.First(c => c.Name.ToLower().StartsWith(request.Uri[0].ToLower()));
            else return null;
        }
    }
}
