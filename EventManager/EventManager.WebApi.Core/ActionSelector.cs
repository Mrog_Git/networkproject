﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using EventManager.WebApi.Interfaces;

namespace EventManager.WebApi.Core
{
    public class ActionSelector: IActionSelector
    {
        public static Dictionary<RequestMethod, string> Conventions { get; private set; }

        public ActionSelector(Dictionary<RequestMethod, string> conventions)
        {
            Conventions = conventions;
        }

        public MethodInfo GetAction(Type controller, HttpRequest request)
        {
            var actions = controller.GetMethods()
                .Where(m => m.GetParameters().Length == request.Parameters.Length);
            if (!actions.Any(m => m.Name.StartsWith(Conventions[request.Method]))) return null;
            return actions.First(m => m.Name.StartsWith(Conventions[request.Method]));
        }
    }
}
