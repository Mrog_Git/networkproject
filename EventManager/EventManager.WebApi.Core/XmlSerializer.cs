﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Text;
using EventManager.WebApi.Interfaces;

namespace EventManager.WebApi.Core
{
    public class XmlSerializer: ISerializer
    {
        public string Serialize(object obj)
        {
            using (MemoryStream memoryStream = new MemoryStream())
            using (StreamReader reader = new StreamReader(memoryStream))
            {
                DataContractSerializer serializer = new DataContractSerializer(obj.GetType());
                serializer.WriteObject(memoryStream, obj);
                memoryStream.Position = 0;
                return reader.ReadToEnd();
            }
        }

        public object Deserialize(string s, Type type)
        {
            using (Stream stream = new MemoryStream())
            {
                byte[] data = Encoding.UTF8.GetBytes(s);
                stream.Write(data, 0, data.Length);
                stream.Position = 0;
                DataContractSerializer deserializer = new DataContractSerializer(type);
                return deserializer.ReadObject(stream);
            }
        }
    }
}
