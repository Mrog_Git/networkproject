﻿using System;
using EventManager.WebApi.Interfaces;
using Newtonsoft.Json;

namespace EventManager.WebApi.Core
{
    public class JsonSerializer : ISerializer
    {
        private readonly JsonSerializerSettings _cfg;

        public JsonSerializer()
        {
            _cfg = new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.All,
                Formatting = Formatting.Indented
            };
        }

        public object Deserialize(string s, Type type)
        {
            return JsonConvert.DeserializeObject(s, type, _cfg);
        }

        public string Serialize(object obj)
        {
            return JsonConvert.SerializeObject(obj, _cfg); 
        }
    }
}
