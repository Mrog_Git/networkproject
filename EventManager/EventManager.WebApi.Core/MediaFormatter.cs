﻿using System;
using System.Collections.Generic;
using EventManager.WebApi.Interfaces;

namespace EventManager.WebApi.Core
{
    public class MediaFormatter: IMediaFormatter
    {
        public IDictionary<string, ISerializer> SerializerMimes { get; }
        public ISerializer DefaultSerializer { get; }


        public MediaFormatter()
        {
            DefaultSerializer = new JsonSerializer();
            
            SerializerMimes = new Dictionary<string, ISerializer>()
            {
                {"application/xml", new XmlSerializer()},
                {"application/json", new JsonSerializer()}
            };
        }

        public string GetResponseMime(string acceptMime)
        {
            if (String.IsNullOrEmpty(acceptMime) || !SerializerMimes.ContainsKey(acceptMime))
                return "application/json";
            return acceptMime;
        }

        public object DeserializeContent(string mime, string content, Type type)
        {
            if (SerializerMimes.ContainsKey(mime)) return SerializerMimes[mime].Deserialize(content, type);
            else throw new Exception("Unsupported type of content.");
        }

        public string SerializeContent(string mime, object obj)
        {
            if (String.IsNullOrEmpty(mime) || !SerializerMimes.ContainsKey(mime))
                return DefaultSerializer.Serialize(obj);
            else return SerializerMimes[mime].Serialize(obj);
        }

    }
}
